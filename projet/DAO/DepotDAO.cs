﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.TextFormatting;

namespace projet
{
   public class DepotDAO  
    {
        public int idDepotDAO;
        public string nomDepotDAO;
        public int adresse_idAdresseDAO;
       
        public DepotDAO(int idDepotDAO, string nomDepotDAO, int adresse_idAdresseDAO)
        {
            this.idDepotDAO = idDepotDAO;
            this.nomDepotDAO = nomDepotDAO;
            this.adresse_idAdresseDAO = adresse_idAdresseDAO;
        }

        public static ObservableCollection<DepotDAO> listeDepot()
        {
            ObservableCollection<DepotDAO> liste = DepotDAL.selectDepots();
            return liste;
        }

        public static DepotDAO getDepot(int idDepot)
        {
            DepotDAO depot = DepotDAL.getDepot(idDepot);
            return depot;
        }
        public static DepotDAO getDepot(string nomDepot)
        {
            DepotDAO depot = DepotDAL.getDepot(nomDepot);
            return depot;
        }

        public static void updateDepot(DepotDAO depot)
        {
            DepotDAL.updateDepot(depot);
        }

        public static void supprimerDepot(int id)
        {
            DepotDAL.supprimerDepot(id);
        }

        public static void insertDepot(DepotDAO depot)
        {
            DepotDAL.insertDepot(depot);
        }
    }



}

