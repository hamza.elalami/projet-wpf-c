﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.TextFormatting;

namespace projet
{
    public class AdresseDAO
    {
        public int idadresseDAO;
        public string regionDAO;
        public string departementDAO;
        public string rueDAO;
        public string villeDAO;
        public int numDAO;

        public AdresseDAO(){ }

        public AdresseDAO(int idadresseDAO, string regionDAO, string departementDAO, string rueDAO, string villeDAO, int numDAO)
        {
            this.idadresseDAO = idadresseDAO;
            this.regionDAO = regionDAO;
            this.departementDAO = departementDAO;
            this.rueDAO = rueDAO;
            this.villeDAO = villeDAO;
            this.numDAO = numDAO;
        }

        public static ObservableCollection<AdresseDAO> listeAdresses()
        {
            ObservableCollection<AdresseDAO> liste = AdresseDAL.selectAdresses();
            return liste;
        }

        public static AdresseDAO getAdresse(int idAdresse)
        {
            AdresseDAO perso = AdresseDAL.getAdresse(idAdresse);
            return perso;
        }

        public static void updateAdresse(AdresseDAO adresse)
        {
            AdresseDAL.updateAdresse(adresse);
        }

        public static void supprimerAdresse(int idAdresse)
        {
            AdresseDAL.supprimerAdresse(idAdresse);
        }

        public static void insertAdresse(AdresseDAO adresse)
        {
            AdresseDAL.insertAdresse(adresse);
        }
    }
}