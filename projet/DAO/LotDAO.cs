﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace projet
{
    class LotDAO
    {
        public int idLot;
        public string nomLot;
        public string descriptionLot;
        public int liquidationJudiciaire;

        public LotDAO(int idLot, string nom, string description,  int liquiJudi)
        {
            this.idLot = idLot;
            this.nomLot = nom;
            this.descriptionLot = description;
            this.liquidationJudiciaire = liquiJudi;
        }

        public static ObservableCollection<LotDAO> listeLot()
        {
            ObservableCollection<LotDAO> to = LotDAL.selectLot();
            return to;
        }

        public static LotDAO GetLot(int idLot)
        {
            LotDAO to = LotDAL.getLot(idLot);
            return to;
        }
        public static LotDAO GetLot(string nomLot)
        {
            LotDAO to = LotDAL.getLot(nomLot);
            return to;
        }


        public static void updateLot(LotDAO to)
        {
            LotDAL.updateLot(to);
        }
        public static void supprimerLot(int id)
        {
            LotDAL.supprimerLot(id);
        }
        public static void insertLot(LotDAO to)
        {
            LotDAL.insertLot(to);
        }
    }
}
