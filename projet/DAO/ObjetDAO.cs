﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class ObjetDAO
    {
        public int idObjet;
        public string nomObjet;
        public string artisteObjet;
        public double prixDepart;
        public double prixFlash;
        public double prixFin;
        public double prixMinEnchere;
        public int vendu;
        public int id_TypeObjet_Objet;
        public int id_Lot_Objet;
        public int id_vendeur_Objet;
        public int id_acheteur_Objet;
        public int id_Depot_Objet;

        public ObjetDAO(int idObjet, string nom, string artiste, double prixdepart, double prixfin, double prixflash, double prixminenchere, int vendu, int idtype, int idlot, int idutilisateur,int idacheteur, int iddepot)
        {
            this.idObjet = idObjet;
            this.nomObjet = nom;
            this.artisteObjet = artiste;
            this.prixDepart = prixdepart;
            this.prixFin = prixfin;
            this.prixFlash = prixflash;
            this.prixMinEnchere = prixminenchere;
            this.vendu = vendu;
            this.id_TypeObjet_Objet = idtype;
            this.id_Lot_Objet = idlot;
            this.id_vendeur_Objet = idutilisateur;
            this.id_acheteur_Objet = idacheteur;
            this.id_Depot_Objet = iddepot;
        }

        public static ObservableCollection<ObjetDAO> listeObjet()
        {
            ObservableCollection<ObjetDAO> to = ObjetDAL.selectObjet();
            return to;
        }

        public static ObjetDAO GetObjet(int idObjet)
        {
            ObjetDAO to = ObjetDAL.getObjet(idObjet);
            return to;
        }

        public static ObjetDAO GetObjet(string nomObjet)
        {
            ObjetDAO to = ObjetDAL.getObjet(nomObjet);
            return to;
        }
        public static void updateObjet(ObjetDAO to)
        {
            ObjetDAL.updateObjet(to);
        }
        public static void supprimerObjet(int id)
        {
            ObjetDAL.supprimerObjet(id);
        }
        public static void insertObjet(ObjetDAO to)
        {
            ObjetDAL.insertObjet(to);
        }
    }
}
