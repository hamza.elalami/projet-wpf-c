﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using System.Text;

namespace projet
{
    class EnchereDAO
    {
        public int idEnchere;
        public double enchere;
        public int id_Enchere_User;
        public int id_Enchere_Objet;

        public EnchereDAO(int idenchere, double enchere, int idEnchere, int idObjet)
        {
            this.idEnchere = idenchere;
            this.enchere = enchere;
            this.id_Enchere_User = idEnchere;
            this.id_Enchere_Objet = idObjet;
        }

        public static ObservableCollection<EnchereDAO> listeEnchere()
        {
            ObservableCollection<EnchereDAO> o = EnchereDAL.selectEnchere();
            return o;
        }

        public static EnchereDAO getEnchere(int idEnchere)
        {
            EnchereDAO en = EnchereDAL.getEnchere(idEnchere);
            return en;
        }

        public static void updateEnchere(EnchereDAO en)
        {
            EnchereDAL.updateEnchere(en);
        }

        public static void supprimerEnchere(int id)
        {
            EnchereDAL.supprimeEnchere(id);
        }

        public static void insertEnchere(EnchereDAO p)
        {
            EnchereDAL.insertEnchere(p);
        }

        //public EnchereDAO()
    }
}
