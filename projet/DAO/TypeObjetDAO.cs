﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http.Headers;
using System.Security.RightsManagement;
using System.Text;

namespace projet
{
    class TypeObjetDAO
    {
        public int idTypeObjet;
        public string typeObjet;
        
        public TypeObjetDAO(int idtypeobjet, string type)
        {
            this.idTypeObjet = idtypeobjet;
            this.typeObjet = type;
        }

        public static ObservableCollection<TypeObjetDAO> listeTypeObjet()
        {
            ObservableCollection<TypeObjetDAO> to = TypeObjetDAL.selectTypeObjet();
            return to;
        }

        public static TypeObjetDAO GetTypeObjet(int idTypeobjet)
        {
            TypeObjetDAO to = TypeObjetDAL.getTypeObjet(idTypeobjet);
            return to;
        }
        public static TypeObjetDAO GetTypeObjet(string nomTypeobjet)
        {
            TypeObjetDAO to = TypeObjetDAL.getTypeObjet(nomTypeobjet);
            return to;
        }
        public static void updateTypeObjet(TypeObjetDAO to)
        {
            TypeObjetDAL.updateTypeTypeObjet(to);
        }
        public static void supprimerTypeObjet(int id)
        {
            TypeObjetDAL.supprimerTypeObjet(id);
        }
        public static void insertTypeObjet(TypeObjetDAO to)
        {
            TypeObjetDAL.insertTypeObjet(to);
        }

    }
}
