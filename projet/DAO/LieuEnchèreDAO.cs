﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.TextFormatting;

namespace projet
{
   public class LieuEnchereDAO  
    {
        public int idLieuEnchereDAO;
        public string nomLieuEnchereDAO;
        public int idAdresseDAO;
        
        public LieuEnchereDAO(int idLieuEnchereDAO, string nomLieuEnchereDAO, int idAdresseDAO)
        {
            this.idLieuEnchereDAO = idLieuEnchereDAO;
            this.nomLieuEnchereDAO = nomLieuEnchereDAO;
            this.idAdresseDAO = idAdresseDAO;    
        }

        public static ObservableCollection<LieuEnchereDAO> listeLieuEnchere()
        {
            ObservableCollection<LieuEnchereDAO> liste = LieuEnchereDAL.selectLieuEncheres();
            return liste;
        }

        public static LieuEnchereDAO getLieuEnchere(int idLieuEnchere)
        {
            LieuEnchereDAO LieuEnchere = LieuEnchereDAL.getLieuEnchere(idLieuEnchere);
            return LieuEnchere;
        }
        public static LieuEnchereDAO getLieuEnchere(string nomLieuEnchere)
        {
            LieuEnchereDAO LieuEnchere = LieuEnchereDAL.getLieuEnchere(nomLieuEnchere);
            return LieuEnchere;
        }

        public static void updateLieuEnchere(LieuEnchereDAO LieuEnchere)
        {
            LieuEnchereDAL.updateLieuEnchere(LieuEnchere);
        }

        public static void supprimerLieuEnchere(int id)
        {
            LieuEnchereDAL.supprimerLieuEnchere(id);
        }

        public static void insertLieuEnchere(LieuEnchereDAO LieuEnchere)
        {
            LieuEnchereDAL.insertLieuEnchere(LieuEnchere);
        }
    }



}

