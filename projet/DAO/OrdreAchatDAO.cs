﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class OrdreAchatDAO
    {
        public double prixMax;
        public int id_Utilisateur;
        public int id_Objet;

        public OrdreAchatDAO(double prix, int idutilisateur, int idobjet)
        {
            this.prixMax = prix;
            this.id_Utilisateur = idutilisateur;
            this.id_Objet = idobjet;
        }

        public static ObservableCollection<OrdreAchatDAO> listeOrdreAchat()
        {
            ObservableCollection<OrdreAchatDAO> to = OrdreAchatDAL.selectOrdreAchat();
            return to;
        }

        public static OrdreAchatDAO GetOrdreAchat(int idUtilisateur, int idObjet)
        {
            OrdreAchatDAO to = OrdreAchatDAL.getOrdreAchat(idUtilisateur,  idObjet);
            return to;
        }

        public static void updateOrdreAchat(OrdreAchatDAO to)
        {
            OrdreAchatDAL.updateOrdreAchat(to);
        }

        public static void supprimerOrdreAchat(int idUtilisateur, int idObjet)
        {
            OrdreAchatDAL.supprimerOrdreAchat(idUtilisateur, idObjet);
        }

        public static void insertOrdreAchat(OrdreAchatDAO to)
        {
            OrdreAchatDAL.insertOrdreAchat(to);
        }
    }

}
