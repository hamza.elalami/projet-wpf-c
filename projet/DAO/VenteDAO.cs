﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class VenteDAO
    {
        public int idVenteDAO;
        public string dateDAO;
        public int lot_idLotDAO;
        public int cp_idCPDAO;
        public int lieuEnchere_idLieuEnchereDAO;

        public VenteDAO(int idVenteDAO, string dateDAO, int lot_idLotDAO, int cp_idCPDAO, int lieuEnchere_idLieuEnchereDAO)
        {
            this.idVenteDAO = idVenteDAO;
            this.dateDAO = dateDAO;
            this.lot_idLotDAO = lot_idLotDAO;
            this.cp_idCPDAO = cp_idCPDAO;
            this.lieuEnchere_idLieuEnchereDAO = lieuEnchere_idLieuEnchereDAO;


        }

        public static ObservableCollection<VenteDAO> listeVente()
        {
            ObservableCollection<VenteDAO> liste = VenteDAL.selectVentes();
            return liste;
        }
        /*
                public static VenteDAO getVente(int idVente, int idAdresse)
                {
                    VenteDAO Vente = VenteDAL.getVente(idVente, idAdresse);
                    return Vente;
                }*/

        public static void updateVente(VenteDAO Vente)
        {
            VenteDAL.updateVente(Vente);
        }

        public static void supprimerVente(int id)
        {
            VenteDAL.supprimerVente(id);
        }

        public static VenteDAO getVente(string nomVente)
        {
            return VenteDAL.getVente(nomVente);
        }
        public static void insertVente(VenteDAO Vente)
        {
            VenteDAL.insertVente(Vente);
        }
    }
}
