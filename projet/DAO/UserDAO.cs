﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.TextFormatting;

namespace projet
{
   public class UserDAO  
    {
        public int idUserDAO;
        public string nomUserDAO;
        public string prenomUserDAO;
        public string DateDeNaissDAO;
        public int NumérodeTelDAO;
        public string AdresseMailDAO;
        public int type;
        public int idAdresseUser;

        public UserDAO(int idUserDAO, string nomUserDAO, string prenomUserDAO, string AdresseMailDAO, int NumérodeTelDAO, int type, int idAdresse, string DateDeNaissDAO)
        {
            this.idUserDAO = idUserDAO;
            this.nomUserDAO = nomUserDAO;
            this.prenomUserDAO = prenomUserDAO;
            this.DateDeNaissDAO = DateDeNaissDAO;
            this.NumérodeTelDAO = NumérodeTelDAO;
            this.AdresseMailDAO = AdresseMailDAO;
            this.type = type;
            this.idAdresseUser = idAdresse;
        }

        public static ObservableCollection<UserDAO> listeUsers()
        {
            ObservableCollection<UserDAO> liste = UserDAL.selectUsers();
            return liste;
        }
        
        public static UserDAO getUser(int idUser)
        {
            UserDAO perso = UserDAL.getUser(idUser);
            return perso;
        }

        public static UserDAO getUser(string nomPrenomUser)
        {
            UserDAO perso = UserDAL.getUser(nomPrenomUser);
            return perso;
        }

        public static void updateUser(UserDAO perso)
        {
            UserDAL.updateUser(perso);
        }

        public static void supprimerUser(int idUser)
        {
            UserDAL.supprimerUser(idUser);
        }

        public static void insertUser(UserDAO perso)
        {
            UserDAL.insertUser(perso);
        }
    }



}

