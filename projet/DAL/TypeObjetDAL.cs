﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;

namespace projet
{
    class TypeObjetDAL
    {
        public static ObservableCollection<TypeObjetDAO> selectTypeObjet()
        {
            ObservableCollection<TypeObjetDAO> o = new ObservableCollection<TypeObjetDAO>();
            string query = "Select * from typeobjet";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    TypeObjetDAO typeObjet = new TypeObjetDAO(reader.GetInt32(0), reader.GetString(1));
                    o.Add(typeObjet);
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table typeobjet : {0}", e.StackTrace);
            }
            reader.Close();
            return o;
        }

        public static void updateTypeTypeObjet(TypeObjetDAO t)
        {
            string query = "update typeobjet set type= \"" + t.typeObjet + "\" where idTypeObjet = " +  t.idTypeObjet ;
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static void insertTypeObjet(TypeObjetDAO t)
        {
            int id = getMaxIdTypeObjet() + 1;
            string query = "Insert into typeobjet values (\"" + id + "\",\""+ t.typeObjet + "\");";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static void supprimerTypeObjet(int id)
        {
            string query = "Delete from typeobjet where idTypeObjet = \"" + id + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static TypeObjetDAO getTypeObjet(int id)
        {
            string query = "select * from typeobjet where idTypeObjet=\"" + id + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            TypeObjetDAO typeObjet = new TypeObjetDAO(reader.GetInt32(0), reader.GetString(1));
            reader.Close();
            return typeObjet;
        }
        public static TypeObjetDAO getTypeObjet(string nomType)
        {
            string query = "select * from typeobjet where type=\"" + nomType + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            TypeObjetDAO typeObjet = new TypeObjetDAO(reader.GetInt32(0), reader.GetString(1));
            reader.Close();
            return typeObjet;
        }
        public static int getMaxIdTypeObjet()
        {
            int maxidTypeObjet = 0;
            string query = "SELECT MAX(idTypeObjet) FROM TypeObjet;";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());

            MySqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Read();
                if (!reader.IsDBNull(0))
                {
                    maxidTypeObjet = reader.GetInt32(0);
                }
                else
                {
                    maxidTypeObjet = 0;
                }
            }
            reader.Close();
            return maxidTypeObjet;
        }
    }
}
