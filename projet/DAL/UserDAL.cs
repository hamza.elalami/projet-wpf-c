﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.Windows.Input;
using MySql.Data.MySqlClient;

namespace projet
{
    public class UserDAL
    {

        public UserDAL() { }
        public static ObservableCollection<UserDAO> selectUsers()
        {
            ObservableCollection<UserDAO> liste = new ObservableCollection<UserDAO>();
            string query = "SELECT * FROM utilisateur";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    UserDAO perso = new UserDAO(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetInt32(4), reader.GetInt32(5), reader.GetInt32(6), reader.GetString(7));
                    liste.Add(perso);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table User : {0}" + e.StackTrace);
            }
            reader.Close();
            return liste;
        }

        public static void updateUser(UserDAO perso)
        {

            string query = "UPDATE utilisateur set nom=\"" + perso.nomUserDAO + "\", prenom=\"" + perso.prenomUserDAO + "\", adresseMail=\"" + perso.AdresseMailDAO + "\", numerTelephone=\"" + perso.NumérodeTelDAO + "\", type=\"" + perso.type + "\", adresse_idAdresse=\"" + perso.idAdresseUser + "\", datenaissuser=\"" + perso.DateDeNaissDAO + "\" where idUtilisateur=" + perso.idUserDAO + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
        }
        public static void insertUser(UserDAO perso)
        {
            //String dateNaissance = p.dateNaisUserDAO.ToString("yyyy-MM-dd");
            string query = "INSERT INTO utilisateur VALUES (\"" + perso.idUserDAO + "\",\"" + perso.nomUserDAO + "\",\"" + perso.prenomUserDAO + "\",\"" + perso.AdresseMailDAO + "\",\"" + perso.NumérodeTelDAO + "\",\"" + perso.type + "\",\"" + perso.idAdresseUser + "\",\"" + perso.DateDeNaissDAO + "\" );";
            MySqlCommand cmd2 = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd2);
            cmd2.ExecuteNonQuery();
        }
        public static void supprimerUser(int idUser)
        {
            string query = " DELETE FROM utilisateur WHERE idUtilisateur = \"" + idUser + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
        }
        // Fonction en stand BY 


        public static int getMaxIdUser()
        {
            int maxIdUser = 0;
            string query = "SELECT MAX(idUtilisateur) FROM utilisateur;";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());

            int cnt = cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            DataTable schemaTable = reader.GetSchemaTable();

            if (reader.HasRows)
            {
                reader.Read();
                if (!reader.IsDBNull(0))
                {
                    maxIdUser = reader.GetInt32(0);
                }
                else
                {
                    maxIdUser = 0;
                }
            }
            reader.Close();
            return maxIdUser;
        }

        public static UserDAO getUser(int idUser)
        {
            string query = "SELECT * FROM utilisateur WHERE idUtilisateur=" + idUser + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            UserDAO use = new UserDAO(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetInt32(4), reader.GetInt32(5), reader.GetInt32(6), reader.GetString(7));
            reader.Close();
            return use;
        }

        public static UserDAO getUser(string nomPrenomUser)
        {
            string[] nom = nomPrenomUser.Split(" ");
            string query = "SELECT * FROM utilisateur WHERE nom=\"" + nom[0] + "\" and prenom= \""+ nom[2] + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            UserDAO use = new UserDAO(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetInt32(4), reader.GetInt32(5), reader.GetInt32(6), reader.GetString(7));
            reader.Close();
            return use;
        }
    }
}