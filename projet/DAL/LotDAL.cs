﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using MySql.Data.MySqlClient;

namespace projet
{
    class LotDAL
    {
        public static ObservableCollection<LotDAO> selectLot()
        {
            ObservableCollection<LotDAO> o = new ObservableCollection<LotDAO>();
            string query = "Select * from Lot";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    LotDAO Lot = new LotDAO(reader.GetInt32(0), reader.GetString(1), reader.GetString(2),  reader.GetInt32(3));
                    o.Add(Lot);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table Lot : {0}", e.StackTrace);
            }
            reader.Close();
            return o;
        }

        public static void updateLot(LotDAO lot)
        {
            string query = "update Lot set nomLot=\"" + lot.nomLot + "\", descriptionLot = \"" + lot.descriptionLot +
                           "\", liquidationJudiciaire = \"" + lot.liquidationJudiciaire + "\" where idLot=" + lot.idLot;
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static void insertLot(LotDAO lot)
        {
            int id = getMaxIdLot() + 1;

            string query = "Insert into Lot values (\"" + id + "\",\"" + lot.nomLot + "\",\"" +
                           lot.descriptionLot + "\",\"" + lot.liquidationJudiciaire +
                           "\");";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static void supprimerLot(int id)
        {
            string query = "Delete from Lot where idLot = \"" + id + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static LotDAO getLot(int id)
        {
            string query = "select * from Lot where idLot=\"" + id + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            LotDAO Lot = new LotDAO(reader.GetInt32(0), reader.GetString(1), reader.GetString(2),  reader.GetInt32(3));
            reader.Close();
            return Lot;
        }
        public static LotDAO getLot(string nomLot)
        {
            string query = "select * from Lot where nomLot=\"" + nomLot + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            LotDAO Lot = new LotDAO(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3));
            reader.Close();
            return Lot;
        }
        public static int getMaxIdLot()
        {
            int maxidLot = 0;
            string query = "SELECT MAX(idLot) FROM Lot;";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());

            MySqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Read();
                if (!reader.IsDBNull(0))
                {
                    maxidLot = reader.GetInt32(0);
                }
                else
                {
                    maxidLot = 0;
                }
            }
            reader.Close();
            return maxidLot;
        }
    }
}
