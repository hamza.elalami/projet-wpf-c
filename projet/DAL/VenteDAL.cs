﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MySql.Data.MySqlClient;

namespace projet
{
    class VenteDAL
    {
        public VenteDAL() { }
        public static ObservableCollection<VenteDAO> selectVentes()
        {
            ObservableCollection<VenteDAO> liste = new ObservableCollection<VenteDAO>();

            string query =
                "SELECT * FROM vente v join lot join utilisateur u join lieuenchere li join adresse a WHERE lot.idlot = v.lot_idLot and u.idUtilisateur = cp_idCP and" + 
                " li.idLieuEnchere = v.lieuEnchere_idLieuEnchere and a.idAdresse = li.adresse_idAdresse";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    VenteDAO Vente = new VenteDAO(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2),
                        reader.GetInt32(3), reader.GetInt32(4));
                    liste.Add(Vente);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table Vente : {0}" + e.StackTrace);
            }
            reader.Close();
            return liste;
        }

        public static void updateVente(VenteDAO Vente)
        {
            string query = "UPDATE vente set  date_Vente=\"" + Vente.dateDAO + "\", lot_idLot=\"" + Vente.lot_idLotDAO +
                           "\", cp_idCP=\"" + Vente.cp_idCPDAO + "\", lieuEnchere_idLieuEnchere=\"" +
                           Vente.lieuEnchere_idLieuEnchereDAO + "\" where idVente=" + Vente.idVenteDAO + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
        }
        public static void insertVente(VenteDAO Vente)
        {
            int idVente = getMaxIdVente() + 1;
            string query = "INSERT INTO vente VALUES (\"" + idVente + "\",\"" + Vente.dateDAO + "\",\"" +
                           Vente.lot_idLotDAO + "\",\"" + Vente.cp_idCPDAO + "\",\"" +
                           Vente.lieuEnchere_idLieuEnchereDAO + "\")";
            MySqlCommand cmd2 = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd2);
            cmd2.ExecuteNonQuery();
        }
        public static void supprimerVente(int idVente)
        {
            string query = " DELETE FROM vente WHERE idVente = \"" + idVente + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
        }
        public static VenteDAO getVente(string nomVente)
        {
            string[] vente = nomVente.Split(" ");
            string query = "SELECT * FROM vente v JOIN lieuenchere l ON v.lieuEnchere_idLieuEnchere=l.idLieuEnchere WHERE v.date_Vente=\"" + vente[2] + "\" and l.nomLieuEnchere= \"" + vente[0] + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            VenteDAO use = new VenteDAO(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4));
            reader.Close();
            return use;
        }
        public static int getMaxIdVente()
        {
            int maxIdVente = 0;
            string query = "SELECT MAX(idVente) FROM vente;";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());

            int cnt = cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            DataTable schemaTable = reader.GetSchemaTable();

            if (reader.HasRows)
            {
                reader.Read();
                if (!reader.IsDBNull(0))
                {
                    maxIdVente = reader.GetInt32(0);
                }
                else
                {
                    maxIdVente = 0;
                }
            }
            reader.Close();
            return maxIdVente;
        }
    }
}
