﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.Windows.Input;
using MySql.Data.MySqlClient;

namespace projet
{
    class AdresseDAL
    {
        public AdresseDAL() { }
        public static ObservableCollection<AdresseDAO> selectAdresses()
        {
            ObservableCollection<AdresseDAO> liste = new ObservableCollection<AdresseDAO>();

            string query = "SELECT * FROM adresse";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdresseDAO adresse = new AdresseDAO(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
                    liste.Add(adresse);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table Adresse : {0}" + e.StackTrace);
            }
            reader.Close();
            return liste;
        }

        public static void updateAdresse(AdresseDAO Adresse)
        {

            string query = "UPDATE Adresse set nomAdresse=\"" + Adresse.regionDAO + "\",\"" + Adresse.departementDAO + "\",\"" + Adresse.villeDAO + "\",\"" + Adresse.rueDAO + "\",\"" + Adresse.numDAO + "\" where idAdresse=" + Adresse.idadresseDAO + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
        }
        public static void insertAdresse(AdresseDAO Adresse)
        {
            string query = "INSERT INTO Adresse VALUES (\"" + Adresse.idadresseDAO + "\",\"" + Adresse.regionDAO + "\",\"" + Adresse.departementDAO + "\",\"" + Adresse.villeDAO + "\",\"" + Adresse.rueDAO + "\",\"" + Adresse.numDAO + "\" );";
            MySqlCommand cmd2 = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd2);
            cmd2.ExecuteNonQuery();
        }
        public static void supprimerAdresse(int idAdresse)
        {
            string query = " DELETE FROM Adresse WHERE idAdresse = \"" + idAdresse + "\"";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
        }


        public static int getMaxIdAdresse()
        {
            int maxIdAdresse = 0;
            string query = "SELECT MAX(idAdresse) FROM adresse;";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());

            int cnt = cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            DataTable schemaTable = reader.GetSchemaTable();

            if (reader.HasRows)
            {
                reader.Read();
                if (!reader.IsDBNull(0))
                {
                    maxIdAdresse = reader.GetInt32(0);
                }
                else
                {
                    maxIdAdresse = 0;
                }
            }
            reader.Close();
            return maxIdAdresse;
        }

        public static AdresseDAO getAdresse(int idAdresse)
        {
            string query = "SELECT * FROM Adresse WHERE idAdresse=" + idAdresse + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            AdresseDAO use = new AdresseDAO(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
            reader.Close();
            return use;
        }
    }
}
