﻿
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.Windows.Input;
using MySql.Data.MySqlClient;

namespace projet
{
    public class LieuEnchereDAL
    {

        public LieuEnchereDAL() { }
        public static ObservableCollection<LieuEnchereDAO> selectLieuEncheres()
        {
            ObservableCollection<LieuEnchereDAO> liste = new ObservableCollection<LieuEnchereDAO>();

            string query = "SELECT * FROM LieuEnchere";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    LieuEnchereDAO LieuEnchere = new LieuEnchereDAO(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
                    liste.Add(LieuEnchere);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table LieuEnchere : {0}" + e.StackTrace);
            }
            reader.Close();
            return liste;
        }

        public static void updateLieuEnchere(LieuEnchereDAO LieuEnchere)
        {

            string query = "UPDATE lieuenchere set nomLieuEnchere=\"" + LieuEnchere.nomLieuEnchereDAO + "\",  adresse_idAdresse=\"" + LieuEnchere.idAdresseDAO + "\" where idLieuEnchere=" + LieuEnchere.idLieuEnchereDAO + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
        }
        public static void insertLieuEnchere(LieuEnchereDAO LieuEnchere)
        {
            string query = "INSERT INTO lieuenchere VALUES (\"" + LieuEnchere.idLieuEnchereDAO + "\",\"" + LieuEnchere.nomLieuEnchereDAO + "\",\"" + LieuEnchere.idAdresseDAO + "\" );";
            MySqlCommand cmd2 = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd2);
            cmd2.ExecuteNonQuery();
        }
        public static void supprimerLieuEnchere(int idLieuEnchere)
        {
            string query = " DELETE FROM lieuenchere WHERE idLieuEnchere = \"" + idLieuEnchere + "\"";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
        }

        public static int getMaxIdLieuEnchere()
        {
            int maxIdLieuEnchere = 0;
            string query = "SELECT MAX(idLieuEnchere) FROM lieuenchere;";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            int cnt = cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            DataTable schemaTable = reader.GetSchemaTable();
            if (reader.HasRows)
            {
                reader.Read();
                if (!reader.IsDBNull(0))
                {
                    maxIdLieuEnchere = reader.GetInt32(0);
                }
                else
                {
                    maxIdLieuEnchere = 0;
                }
            }
            reader.Close();
            return maxIdLieuEnchere;
        }

        public static LieuEnchereDAO getLieuEnchere(string nomLieuEnchere)
        {
            string query = "SELECT * FROM lieuenchere where nomLieuEnchere=\"" + nomLieuEnchere + "\"";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            LieuEnchereDAO use = new LieuEnchereDAO(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
            reader.Close();
            return use;
        }

        public static LieuEnchereDAO getLieuEnchere(int idLieuEnchere)
        {
            string query = "SELECT * FROM lieuenchere where idLieuEnchere=\"" + idLieuEnchere + "\"";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            LieuEnchereDAO use = new LieuEnchereDAO(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
            reader.Close();
            return use;
        }
    }
}
