﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Text;
using System.Windows;

namespace projet
{
    class EnchereDAL
    {
        public static ObservableCollection<EnchereDAO> selectEnchere()
        {
            ObservableCollection<EnchereDAO> o = new ObservableCollection<EnchereDAO>();
            string query = "Select * from encheredirecte";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EnchereDAO u = new EnchereDAO(reader.GetInt32(0), reader.GetDouble(1), reader.GetInt32(2), reader.GetInt32(3));
                    o.Add(u);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table enchere : {0}", e.StackTrace);
            }
            reader.Close();
            return o;
        }
        public static void updateEnchere(EnchereDAO p)
        {
            string query = "update encheredirecte set encherePrix=\"" + p.enchere + "\");";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static void insertEnchere(EnchereDAO p)
        {
            int id = getMaxIdEnchere() + 1;
            string query = "Insert into encheredirecte values (\"" + id + "\",\"" + p.enchere.ToString().Replace(",", ".") + "\",\"" + p.id_Enchere_User + "\",\"" + p.id_Enchere_Objet +"\");";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static void supprimeEnchere(int id)
        {
            string query = "delete from encheredirecte where idEnchere = \"" + id + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static EnchereDAO getEnchere(int idEnchere)
        {
            string query = "select * from encheredirecte where id=" + idEnchere + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            EnchereDAO enchere = new EnchereDAO(reader.GetInt32(0), reader.GetDouble(1), reader.GetInt32(2), reader.GetInt32(3));
            reader.Close();
            return enchere;
        }

        public static int getMaxIdEnchere()
        {
            int maxidEnchere = 0;
            string query = "SELECT MAX(idEnchere) FROM encheredirecte;";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());

            MySqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Read();
                if (!reader.IsDBNull(0))
                {
                    maxidEnchere = reader.GetInt32(0);
                }
                else
                {
                    maxidEnchere = 0;
                }
            }
            reader.Close();
            return maxidEnchere;
        }
    }
}
