﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MySql.Data.MySqlClient;

namespace projet
{
    class OrdreAchatDAL
    {
        public static ObservableCollection<OrdreAchatDAO> selectOrdreAchat()
        {
            ObservableCollection<OrdreAchatDAO> o = new ObservableCollection<OrdreAchatDAO>();
            string query = "Select * from OrdreAchat";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    OrdreAchatDAO OrdreAchat = new OrdreAchatDAO(reader.GetDouble(0), reader.GetInt32(1), reader.GetInt32(2));
                    o.Add(OrdreAchat);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table OrdreAchat : {0}", e.StackTrace);
            }
            reader.Close();
            return o;
        }

        public static void updateOrdreAchat(OrdreAchatDAO OrdreAchat)
        {
            string query = "update OrdreAchat set prixMax=\"" + OrdreAchat.prixMax + "\" where utilisateur_idUtilisateur =" + OrdreAchat.id_Utilisateur + "\" AND objet_idObjet = \"" + OrdreAchat.id_Objet + ")";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static void insertOrdreAchat(OrdreAchatDAO OrdreAchat)
        {

            string query = "Insert into OrdreAchat values (\"" + OrdreAchat.prixMax + "\",\"" + OrdreAchat.id_Utilisateur + "\",\"" + OrdreAchat.id_Objet + "\");";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static void supprimerOrdreAchat(int idUtilisateur, int idObjet)
        {
            string query = "Delete from OrdreAchat where utilisateur_idUtilisateur  = \"" + idUtilisateur + "\" AND objet_idObjet =\"" + idObjet + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static OrdreAchatDAO getOrdreAchat(int idUtilisateur, int idObjet)
        {
            string query = "select * from OrdreAchat OrdreAchat where utilisateur_idUtilisateur  = \"" + idUtilisateur + "\" AND objet_idObjet =\"" + idObjet + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            OrdreAchatDAO OrdreAchat = new OrdreAchatDAO(reader.GetDouble(0), reader.GetInt32(1), reader.GetInt32(2));
            reader.Close();
            return OrdreAchat;
        }
    }
}
