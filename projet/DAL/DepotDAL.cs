﻿
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.Windows.Input;
using MySql.Data.MySqlClient;

namespace projet
{
    public class DepotDAL
    {

        public static ObservableCollection<DepotDAO> selectDepots()
        {
            ObservableCollection<DepotDAO> liste = new ObservableCollection<DepotDAO>();

            string query = "SELECT * FROM depot";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    DepotDAO depot = new DepotDAO(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
                    liste.Add(depot);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table Depot : {0}" + e.StackTrace);
            }
            reader.Close();
            return liste;
        }

        public static void updateDepot(DepotDAO depot)
        {

            string query = "UPDATE depot set nom_Depot=\"" + depot.nomDepotDAO + "\",  adresse_idAdresse=\"" + depot.adresse_idAdresseDAO + "\" where idDepot=" + depot.idDepotDAO + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
        }
        public static void insertDepot(DepotDAO depot)
        {
            string query = "INSERT INTO depot VALUES (\"" + depot.idDepotDAO + "\",\"" + depot.nomDepotDAO + "\",\"" + depot.adresse_idAdresseDAO + "\" );";
            MySqlCommand cmd2 = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd2);
            cmd2.ExecuteNonQuery();
        }
        public static void supprimerDepot(int idDepot)
        {
            string query = " DELETE FROM depot WHERE idDepot = \"" + idDepot + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataAdapter sqlDataAdap = new MySqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
        }
        // Fonction en stand BY 


        public static int getMaxIdDepot()
        {
            int maxIdDepot = 0;
            string query = "SELECT MAX(idDepot) FROM depot;";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());

            int cnt = cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            DataTable schemaTable = reader.GetSchemaTable();

            if (reader.HasRows)
            {
                reader.Read();
                if (!reader.IsDBNull(0))
                {
                    maxIdDepot = reader.GetInt32(0);
                }
                else
                {
                    maxIdDepot = 0;
                }
            }
            reader.Close();
            return maxIdDepot;
        }


        public static DepotDAO getDepot(int idDepot)
        {
            int id = idDepot;
            string query = "SELECT * FROM depot WHERE idDepot=" + idDepot + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            DepotDAO use = new DepotDAO(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
            reader.Close();
            return use;
        }
        public static DepotDAO getDepot(string nomDepot)
        {
            string query = "SELECT * FROM depot WHERE nom_Depot=\"" + nomDepot + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            DepotDAO use = new DepotDAO(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
            reader.Close();
            return use;
        }
    }
}