﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using MySql.Data.MySqlClient;

namespace projet
{
    class ObjetDAL
    {

        public static ObservableCollection<ObjetDAO> selectObjet()
        {
            ObservableCollection<ObjetDAO> o = new ObservableCollection<ObjetDAO>();
            string query = "Select * from objet";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ObjetDAO Objet = new ObjetDAO(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetDouble(3),reader.GetDouble(4),
                        reader.GetDouble(5),reader.GetInt32(6),reader.GetInt32(7),reader.GetInt32(8), reader.GetInt32(9),
                        reader.GetInt32(10),reader.GetInt32(11), reader.GetInt32(12));
                    o.Add(Objet);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table Objet : {0}", e.StackTrace);
            }
            reader.Close();
            return o;
        }


        public static void updateObjet(ObjetDAO Objet)
        {
            string query = "update Objet set nomObjet=\"" + Objet.nomObjet + "\", artisteObjet = \"" + Objet.artisteObjet + "\", prixDepart = \"" + Objet.prixDepart.ToString().Replace(",",".")
                           + "\", prixFin = \"" + Objet.prixFin.ToString().Replace(",", ".") + "\", prixFlash = \"" + Objet.prixFlash.ToString().Replace(",", ".")
                           + "\", vendu = \"" + Objet.vendu + "\", typeObjet_idTypeObjet = \"" + Objet.id_TypeObjet_Objet + "\", lot_idlot = \"" + Objet.id_Lot_Objet + "\", vendeur_idVendeur = \""
                           + Objet.id_vendeur_Objet + "\", acheteur_idAcheteur = \"" + Objet.id_acheteur_Objet + "\", depot_idDepot = \"" + Objet.id_Depot_Objet +   "\" where idObjet=" + Objet.idObjet;
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static void insertObjet(ObjetDAO Objet)
        {
            int id = getMaxIdObjet() + 1;
            string query = "Insert into Objet values (\"" + id + "\",\"" + Objet.nomObjet + "\",\"" + Objet.artisteObjet + "\",\""+ Objet.prixDepart.ToString().Replace(",", ".")
                           + "\",\""+ Objet.prixFin.ToString().Replace(",", ".") + "\",\""+ Objet.prixFlash.ToString().Replace(",", ".") + "\",\""
                           + Objet.prixMinEnchere.ToString().Replace(",", ".") + "\",\"" + Objet.vendu + "\",\""+ Objet.id_TypeObjet_Objet + "\",\""
                           + Objet.id_Lot_Objet + "\",\"" + Objet.id_vendeur_Objet + "\",\"" + Objet.id_acheteur_Objet + "\",\"" + Objet.id_Depot_Objet + "\");";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static void supprimerObjet(int id)
        {
            string query = "Delete from Objet where idObjet = \"" + id + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
        }

        public static ObjetDAO getObjet(int id)
        {
            string query = "select * from Objet Objet where idObjet=\"" + id + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            ObjetDAO Objet = new ObjetDAO(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetDouble(3), reader.GetDouble(4),
                reader.GetDouble(5), reader.GetInt32(6), reader.GetInt32(7), reader.GetInt32(8), reader.GetInt32(9),
                reader.GetInt32(10), reader.GetInt32(11), reader.GetInt32(12));
            reader.Close();
            return Objet;
        }

        public static ObjetDAO getObjet(string nomObjet)
        {
            string query = "select * from Objet Objet where nomObjet=\"" + nomObjet + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            ObjetDAO Objet = new ObjetDAO(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetDouble(3), reader.GetDouble(4),
                reader.GetDouble(5), reader.GetInt32(6), reader.GetInt32(7), reader.GetInt32(8), reader.GetInt32(9),
                reader.GetInt32(10), reader.GetInt32(11), reader.GetInt32(12));
            reader.Close();
            return Objet;
        }

        public static int getMaxIdObjet()
        {
            int maxidObjet = 0;
            string query = "SELECT MAX(idObjet) FROM Objet;";
            MySqlCommand cmd = new MySqlCommand(query, DALconnection.OpenConnection());

            MySqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Read();
                if (!reader.IsDBNull(0))
                {
                    maxidObjet = reader.GetInt32(0);
                }
                else
                {
                    maxidObjet = 0;
                }
            }
            reader.Close();
            return maxidObjet;
        }
    }
}
