﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class OrdreAchatORM
    {
        public static OrdreAchatViewModel getOrdreAchat(int idUtilisateur, int idObjet)
        {
            OrdreAchatDAO OrdreAchatDao = OrdreAchatDAO.GetOrdreAchat(idUtilisateur, idObjet);
            UserViewModel user = UserORM.getUser(OrdreAchatDao.id_Utilisateur);
            ObjetViewModel objet = ObjetORM.getObjet(OrdreAchatDao.id_Objet);

            OrdreAchatViewModel to = new OrdreAchatViewModel(OrdreAchatDao.prixMax, user, objet);
            return to;
        }

        public static ObservableCollection<OrdreAchatViewModel> listeOrdreAchat()
        {
            ObservableCollection<OrdreAchatDAO> OrdreAchatDao = OrdreAchatDAO.listeOrdreAchat();
            ObservableCollection<OrdreAchatViewModel> toVM = new ObservableCollection<OrdreAchatViewModel>();
            foreach (OrdreAchatDAO e in OrdreAchatDao)
            {
                OrdreAchatViewModel OrdreAchatViewModel = new OrdreAchatViewModel(e.prixMax, UserORM.getUser(e.id_Utilisateur), ObjetORM.getObjet(e.id_Objet));
                toVM.Add(OrdreAchatViewModel);
            }
            return toVM;
        }

        public static void updateOrdreAchat(OrdreAchatViewModel OrdreAchat)
        {
            OrdreAchatDAO.updateOrdreAchat(new OrdreAchatDAO(OrdreAchat.prixMaxProperty, OrdreAchat.userOAProperty.idUserProperty, OrdreAchat.objetOAProperty.idObjetProperty));
        }

        public static void supprimerOrdreAchat(int idUtilisateur, int idObjet)
        {
            OrdreAchatDAO.supprimerOrdreAchat(idUtilisateur, idObjet);
        }
        public static void insertOrdreAchat(OrdreAchatViewModel OrdreAchat)
        {
            OrdreAchatDAO.insertOrdreAchat(new OrdreAchatDAO(OrdreAchat.prixMaxProperty, OrdreAchat.userOAProperty.idUserProperty, OrdreAchat.objetOAProperty.idObjetProperty));
        }
    }
}
