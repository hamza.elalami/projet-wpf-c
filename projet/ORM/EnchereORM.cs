﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace projet
{
    class EnchereORM
    {
        public static EnchereViewModel getEnchere(int idEnchere)
        {
            EnchereDAO enchereDAO = EnchereDAO.getEnchere(idEnchere);
            EnchereViewModel u = new EnchereViewModel(enchereDAO.idEnchere, enchereDAO.enchere, UserORM.getUser(enchereDAO.id_Enchere_User), ObjetORM.getObjet(enchereDAO.id_Enchere_Objet));
            return u;
        }

        public static ObservableCollection<EnchereViewModel> listeEnchere()
        {
            ObservableCollection<EnchereDAO> eDao = EnchereDAO.listeEnchere();
            ObservableCollection<EnchereViewModel> eVM = new ObservableCollection<EnchereViewModel>();
            foreach(EnchereDAO e in eDao)
            {
                EnchereViewModel enchereViewModel = new EnchereViewModel(e.idEnchere, e.enchere, UserORM.getUser(e.id_Enchere_User), ObjetORM.getObjet(e.id_Enchere_Objet));
                eVM.Add(enchereViewModel);
            }
            return eVM;
        }

        public static void updateEnchere(EnchereViewModel Enchere)
        {
            EnchereDAO.updateEnchere(new EnchereDAO(Enchere.idEnchereProperty, Enchere.enchereProperty, Enchere.userEnchereProperty.idUserProperty, Enchere.objetEnchereProperty.idObjetProperty));
        }

        public static void supprimerEnchere(int id)
        {
            EnchereDAO.supprimerEnchere(id);
        }
        public static void insertEnchere(EnchereViewModel Enchere)
        {
            EnchereDAO.insertEnchere(new EnchereDAO(Enchere.idEnchereProperty, Enchere.enchereProperty, Enchere.userEnchereProperty.idUserProperty, Enchere.objetEnchereProperty.idObjetProperty));
        }
    }
}
