﻿

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace projet
{
    public class LieuEnchereORM
    { 
        public static LieuEnchereViewModel getLieuEnchere(int idLieuEnchere)
        {
            LieuEnchereDAO LieuEnchereDAO = LieuEnchereDAO.getLieuEnchere(idLieuEnchere);
            LieuEnchereViewModel LieuEnchere = new LieuEnchereViewModel(LieuEnchereDAO.idLieuEnchereDAO, LieuEnchereDAO.nomLieuEnchereDAO, AdresseORM.getAdresse(LieuEnchereDAO.idAdresseDAO));
            return LieuEnchere;
        }
        public static LieuEnchereViewModel getLieuEnchere(string nomLieuEnchere)
        {
            LieuEnchereDAO LieuEnchereDAO = LieuEnchereDAO.getLieuEnchere(nomLieuEnchere);
            LieuEnchereViewModel LieuEnchere = new LieuEnchereViewModel(LieuEnchereDAO.idLieuEnchereDAO, LieuEnchereDAO.nomLieuEnchereDAO, AdresseORM.getAdresse(LieuEnchereDAO.idAdresseDAO));
            return LieuEnchere;
        }

        public static ObservableCollection<LieuEnchereViewModel> listeLieuEnchere()
        {
            ObservableCollection<LieuEnchereDAO> listeDAO = LieuEnchereDAO.listeLieuEnchere();
            ObservableCollection<LieuEnchereViewModel> liste = new ObservableCollection<LieuEnchereViewModel>();
            foreach (LieuEnchereDAO element in listeDAO)
            {
                LieuEnchereViewModel LieuEnchere = new LieuEnchereViewModel(element.idLieuEnchereDAO, element.nomLieuEnchereDAO, AdresseORM.getAdresse(element.idAdresseDAO));
                liste.Add(LieuEnchere);
            }
            return liste;
        }


        public static void updateLieuEnchere(LieuEnchereViewModel LieuEnchere)
        {
            LieuEnchereDAO.updateLieuEnchere(new LieuEnchereDAO(LieuEnchere.idLieuEnchereProperty, LieuEnchere.nomLieuEnchereProperty, LieuEnchere.adresseLieuEnchereProperty.idAdresseProperty));
        }

        public static void supprimerLieuEnchere(int id)
        {
            LieuEnchereDAO.supprimerLieuEnchere(id);
        }

        public static void insertLieuEnchere(LieuEnchereViewModel LieuEnchere)
        {
            LieuEnchereDAO.insertLieuEnchere(new LieuEnchereDAO(LieuEnchere.idLieuEnchereProperty, LieuEnchere.nomLieuEnchereProperty, LieuEnchere.adresseLieuEnchereProperty.idAdresseProperty));
        }
    }
}
