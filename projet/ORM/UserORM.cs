﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace projet
{
    public class UserORM
    {
        
        public static UserViewModel getUser(int idUser)
        {
            UserDAO persoDAO = UserDAO.getUser(idUser);
            
            UserViewModel perso = new UserViewModel(persoDAO.idUserDAO, persoDAO.nomUserDAO, persoDAO.prenomUserDAO,persoDAO.AdresseMailDAO,  persoDAO.NumérodeTelDAO  ,persoDAO.type, persoDAO.DateDeNaissDAO, AdresseORM.getAdresse(persoDAO.idAdresseUser));
            return perso;
        }
        public static UserViewModel getUser(string nomPrenomUser)
        {
            UserDAO persoDAO = UserDAO.getUser(nomPrenomUser);

            UserViewModel perso = new UserViewModel(persoDAO.idUserDAO, persoDAO.nomUserDAO, persoDAO.prenomUserDAO, persoDAO.AdresseMailDAO, persoDAO.NumérodeTelDAO, persoDAO.type, persoDAO.DateDeNaissDAO, AdresseORM.getAdresse(persoDAO.idAdresseUser));
            return perso;
        }

        public static ObservableCollection<UserViewModel> listeUsers()
        {
            ObservableCollection<UserDAO> listeDAO = UserDAO.listeUsers();
            ObservableCollection<UserViewModel> liste = new ObservableCollection<UserViewModel>();
            foreach (UserDAO element in listeDAO)
            {
                UserViewModel perso = new UserViewModel(element.idUserDAO, element.nomUserDAO, element.prenomUserDAO, element.AdresseMailDAO, element.NumérodeTelDAO, element.type, element.DateDeNaissDAO, AdresseORM.getAdresse(element.idAdresseUser));
                liste.Add(perso);
            }
            return liste;
        }


        public static void updateUser(UserViewModel perso)
        {
            UserDAO.updateUser(new UserDAO(perso.idUserProperty, perso.nomUserProperty, perso.prenomUserProperty, perso.AdresseMailUserProperty,perso.NuméroDeTelUserProperty, perso.TypeUserProperty, perso.adresseUserProperty.idAdresseProperty, perso.DateDeNaissUserProperty));
        }

        public static void supprimerUser(int idUser)
        {
            UserDAO.supprimerUser(idUser);
        }

        public static void insertUser(UserViewModel perso)
        {
            UserDAO.insertUser(new UserDAO(perso.idUserProperty, perso.nomUserProperty, perso.prenomUserProperty, perso.AdresseMailUserProperty, perso.NuméroDeTelUserProperty, perso.TypeUserProperty, perso.adresseUserProperty.idAdresseProperty, perso.DateDeNaissUserProperty));
        }
    }





}

