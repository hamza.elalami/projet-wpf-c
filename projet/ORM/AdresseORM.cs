﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class AdresseORM
    {
        public static AdresseViewModel getAdresse(int idAdresse)
        {
            AdresseDAO AdresseDAO = AdresseDAO.getAdresse(idAdresse);
            AdresseViewModel Adresse = new AdresseViewModel(AdresseDAO.idadresseDAO, AdresseDAO.regionDAO, AdresseDAO.departementDAO, AdresseDAO.villeDAO, AdresseDAO.rueDAO, AdresseDAO.numDAO);
            return Adresse;
        }

        public static ObservableCollection<AdresseViewModel> listeAdresses()
        {
            ObservableCollection<AdresseDAO> listeDAO = AdresseDAO.listeAdresses();
            ObservableCollection<AdresseViewModel> liste = new ObservableCollection<AdresseViewModel>();
            foreach (AdresseDAO element in listeDAO)
            {
                AdresseViewModel Adresse = new AdresseViewModel(element.idadresseDAO, element.regionDAO, element.departementDAO, element.villeDAO, element.rueDAO, element.numDAO);
                liste.Add(Adresse);
            }
            return liste;
        }


        public static void updateAdresse(AdresseViewModel Adresse)
        {
            AdresseDAO.updateAdresse(new AdresseDAO(Adresse.idAdresseProperty, Adresse.regionProperty, Adresse.departementProperty, Adresse.villeProperty, Adresse.rueProperty, Adresse.numProperty));
        }

        public static void supprimerAdresse(int idAdresse)
        {
            AdresseDAO.supprimerAdresse(idAdresse);
        }

        public static void insertAdresse(AdresseViewModel Adresse)
        {
            AdresseDAO.insertAdresse(new AdresseDAO(Adresse.idAdresseProperty, Adresse.regionProperty, Adresse.departementProperty, Adresse.villeProperty, Adresse.rueProperty, Adresse.numProperty));
        }
    }
}
