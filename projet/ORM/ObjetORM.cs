﻿using System.Collections.ObjectModel;


namespace projet
{
    class ObjetORM
    {

        public static ObjetViewModel getObjet(int id)
        {
            ObjetDAO ObjetDao = ObjetDAO.GetObjet(id);
            int idType = ObjetDao.id_TypeObjet_Objet;
            TypeObjetViewModel typeObjet = TypeObjetORM.getTypeObjet(idType);
            int idLot = ObjetDao.id_Lot_Objet;
            LotViewModel lotObjet = LotORM.getLot(idLot);
            int idVendeur = ObjetDao.id_vendeur_Objet;
            UserViewModel vendeurObjet = UserORM.getUser(idVendeur);
            int idAcheteur = ObjetDao.id_acheteur_Objet;
            UserViewModel acheteurObjet = UserORM.getUser(idAcheteur);
            int idDepot = ObjetDao.id_Depot_Objet;
            DepotViewModel depotObjet = DepotORM.getDepot(idDepot);

            ObjetViewModel to = new ObjetViewModel(ObjetDao.idObjet, ObjetDao.nomObjet, ObjetDao.artisteObjet, ObjetDao.prixDepart, ObjetDao.prixFin, ObjetDao.prixFlash,ObjetDao.prixMinEnchere, ObjetDao.vendu, typeObjet, lotObjet, vendeurObjet, acheteurObjet, depotObjet);
            return to;
        }
        public static ObjetViewModel getObjet(string nomObjet)
        {
            ObjetDAO ObjetDao = ObjetDAO.GetObjet(nomObjet);
            int idType = ObjetDao.id_TypeObjet_Objet;
            TypeObjetViewModel typeObjet = TypeObjetORM.getTypeObjet(idType);
            int idLot = ObjetDao.id_Lot_Objet;
            LotViewModel lotObjet = LotORM.getLot(idLot);
            int idVendeur = ObjetDao.id_vendeur_Objet;
            UserViewModel vendeurObjet = UserORM.getUser(idVendeur);
            int idAcheteur = ObjetDao.id_acheteur_Objet;
            UserViewModel acheteurObjet = UserORM.getUser(idAcheteur);
            int idDepot = ObjetDao.id_Depot_Objet;
            DepotViewModel depotObjet = DepotORM.getDepot(idDepot);

            ObjetViewModel to = new ObjetViewModel(ObjetDao.idObjet, ObjetDao.nomObjet, ObjetDao.artisteObjet, ObjetDao.prixDepart, ObjetDao.prixFin, ObjetDao.prixFlash, ObjetDao.prixMinEnchere, ObjetDao.vendu, typeObjet, lotObjet, vendeurObjet, acheteurObjet, depotObjet);
            return to;
        }

        public static ObservableCollection<ObjetViewModel> listeObjet()
        {
            ObservableCollection<ObjetDAO> ObjetDao = ObjetDAO.listeObjet();
            ObservableCollection<ObjetViewModel> toVM = new ObservableCollection<ObjetViewModel>();
            foreach (ObjetDAO e in ObjetDao)
            {
                int idType = e.id_TypeObjet_Objet ;
                TypeObjetViewModel typeObjet = TypeObjetORM.getTypeObjet(idType);
                int idLot = e.id_Lot_Objet;
                LotViewModel lotObjet = LotORM.getLot(idLot);
                int idVendeur = e.id_vendeur_Objet;
                UserViewModel vendeurObjet = UserORM.getUser(idVendeur);
                int idAcheteur = e.id_acheteur_Objet;
                UserViewModel acheteurObjet = UserORM.getUser(idAcheteur);
                int idDepot = e.id_Depot_Objet;
                DepotViewModel depotObjet = DepotORM.getDepot(idDepot);
                ObjetViewModel ObjetViewModel = new ObjetViewModel(e.idObjet, e.nomObjet, e.artisteObjet, e.prixDepart, e.prixFin, e.prixFlash,e.prixMinEnchere, e.vendu, typeObjet, lotObjet, vendeurObjet, acheteurObjet, depotObjet);
                toVM.Add(ObjetViewModel);
            }
            return toVM;
        }

        public static void updateObjet(ObjetViewModel Objet)
        {
            ObjetDAO.updateObjet(new ObjetDAO(Objet.idObjetProperty, Objet.nomObjetProperty, Objet.artisteObjetProperty, Objet.prixDepartProperty, Objet.prixfinProperty, Objet.prixFlashProperty, Objet.prixMinEnchereProperty, Objet.venduProperty, Objet.typeObjetProperty.idTOProperty, Objet.lotObjetProperty.idLotProperty, Objet.vendeurObjetProperty.idUserProperty, Objet.acheteurObjetProperty.idUserProperty, Objet.depotObjetProperty.idDepotProperty));
        }

        public static void supprimerObjet(int id)
        {
            ObjetDAO.supprimerObjet(id);
        }
        public static void insertObjet(ObjetViewModel Objet)
        {
            ObjetDAO.insertObjet(new ObjetDAO(Objet.idObjetProperty, Objet.nomObjetProperty, Objet.artisteObjetProperty, Objet.prixDepartProperty, Objet.prixfinProperty, Objet.prixFlashProperty, Objet.prixMinEnchereProperty, Objet.venduProperty, Objet.typeObjetProperty.idTOProperty, Objet.lotObjetProperty.idLotProperty, Objet.vendeurObjetProperty.idUserProperty, 0, Objet.depotObjetProperty.idDepotProperty));
        }
    }
}
