﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace projet
{
    class TypeObjetORM
    {
        public static TypeObjetViewModel getTypeObjet(int id)
        {
            TypeObjetDAO typeObjetDao = TypeObjetDAO.GetTypeObjet(id);
            TypeObjetViewModel to = new TypeObjetViewModel(typeObjetDao.idTypeObjet, typeObjetDao.typeObjet);
            return to;
        }
        public static TypeObjetViewModel getTypeObjet(string nomType)
        {
            TypeObjetDAO typeObjetDao = TypeObjetDAO.GetTypeObjet(nomType);
            TypeObjetViewModel to = new TypeObjetViewModel(typeObjetDao.idTypeObjet, typeObjetDao.typeObjet);
            return to;
        }

        public static ObservableCollection<TypeObjetViewModel> listeTypeObjet()
        {
            ObservableCollection<TypeObjetDAO> toDao = TypeObjetDAO.listeTypeObjet();
            ObservableCollection<TypeObjetViewModel> toVM = new ObservableCollection<TypeObjetViewModel>();
            foreach (TypeObjetDAO e in toDao)
            {
                TypeObjetViewModel typeObjetViewModel = new TypeObjetViewModel(e.idTypeObjet, e.typeObjet);
                toVM.Add(typeObjetViewModel);

            }

            return toVM;
        }

        public static void updateTypeObjet(TypeObjetViewModel TypeObjet)
        {
            TypeObjetDAO.updateTypeObjet(new TypeObjetDAO(TypeObjet.idTOProperty, TypeObjet.typeObjetProperty));
        }

        public static void supprimerTypeObjet(int id)
        {
            TypeObjetDAO.supprimerTypeObjet(id);
        }
        public static void insertTypeObjet(TypeObjetViewModel TypeObjet)
        {
            TypeObjetDAO.insertTypeObjet(new TypeObjetDAO(TypeObjet.idTOProperty, TypeObjet.typeObjetProperty));
        }
    }
}
