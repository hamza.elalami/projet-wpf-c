﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace projet
{
    public class DepotORM
    { 
        public static DepotViewModel getDepot(int idDepot)
        {
            DepotDAO depotDAO = DepotDAO.getDepot(idDepot);
            DepotViewModel depot = new DepotViewModel(depotDAO.idDepotDAO, depotDAO.nomDepotDAO, AdresseORM.getAdresse(depotDAO.adresse_idAdresseDAO));
            return depot;
        }
        public static DepotViewModel getDepot(string nomDepot)
        {
            DepotDAO depotDAO = DepotDAO.getDepot(nomDepot);
            DepotViewModel depot = new DepotViewModel(depotDAO.idDepotDAO, depotDAO.nomDepotDAO, AdresseORM.getAdresse(depotDAO.adresse_idAdresseDAO));
            return depot;
        }

        public static ObservableCollection<DepotViewModel> listeDepot()
        {
            ObservableCollection<DepotDAO> listeDAO = DepotDAO.listeDepot();
            ObservableCollection<DepotViewModel> liste = new ObservableCollection<DepotViewModel>();
            foreach (DepotDAO element in listeDAO)
            {
                DepotViewModel depot = new DepotViewModel(element.idDepotDAO, element.nomDepotDAO, AdresseORM.getAdresse(element.adresse_idAdresseDAO));
                liste.Add(depot);
            }
            return liste;
        }


        public static void updateDepot(DepotViewModel depot)
        {
            DepotDAO.updateDepot(new DepotDAO(depot.idDepotProperty, depot.nomDepotProperty, depot.adresseDepotProperty.idAdresseProperty));
        }

        public static void supprimerDepot(int id)
        {
            DepotDAO.supprimerDepot(id);
        }

        public static void insertDepot(DepotViewModel depot)
        {
            DepotDAO.insertDepot(new DepotDAO(depot.idDepotProperty, depot.nomDepotProperty, depot.adresseDepotProperty.idAdresseProperty));
        }
    }





}

