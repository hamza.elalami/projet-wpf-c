﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class VenteORM
    {
        public static ObservableCollection<VenteViewModel> listeVente()
        {
            ObservableCollection<VenteDAO> listeDAO = VenteDAO.listeVente();
            ObservableCollection<VenteViewModel> liste = new ObservableCollection<VenteViewModel>();
            foreach (VenteDAO element in listeDAO)
            {
                int idLot = element.lot_idLotDAO;
                LotViewModel lot = LotORM.getLot(idLot);
                int idCp = element.cp_idCPDAO;
                UserViewModel cp = UserORM.getUser(idCp);
                int idLieu = element.lieuEnchere_idLieuEnchereDAO;
                LieuEnchereViewModel lieu = LieuEnchereORM.getLieuEnchere(idLieu);
                VenteViewModel Vente = new VenteViewModel(element.idVenteDAO, element.dateDAO, lot, cp, lieu);
                liste.Add(Vente);
            }
            return liste;
        }

        public static VenteViewModel getVente(string nomVente)
        {
            VenteDAO vente = VenteDAO.getVente(nomVente);
            VenteViewModel venteView =  new VenteViewModel(vente.idVenteDAO, vente.dateDAO, LotORM.getLot(vente.lot_idLotDAO), UserORM.getUser(vente.cp_idCPDAO), LieuEnchereORM.getLieuEnchere(vente.lieuEnchere_idLieuEnchereDAO));
            return venteView;
        }

            
        public static void updateVente(VenteViewModel Vente)
        {
            VenteDAO.updateVente(new VenteDAO(Vente.idVenteProperty, Vente.dateVenteProperty, Vente.lotVenteProperty.idLotProperty, Vente.cpVenteProperty.idUserProperty, Vente.lieuVenteProperty.adresseLieuEnchereProperty.idAdresseProperty));
        }

        public static void supprimerVente(int id)
        {
            VenteDAO.supprimerVente(id);
        }

        public static void insertVente(VenteViewModel Vente)
        {
            VenteDAO.insertVente(new VenteDAO(Vente.idVenteProperty, Vente.dateVenteProperty, Vente.lotVenteProperty.idLotProperty, Vente.cpVenteProperty.idUserProperty, Vente.lieuVenteProperty.idLieuEnchereProperty));
        }
    }

}

