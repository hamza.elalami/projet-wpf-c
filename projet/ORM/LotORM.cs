﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace projet
{
    class LotORM
    {
        public static LotViewModel getLot(int id)
        {
            LotDAO LotDao = LotDAO.GetLot(id);
            LotViewModel to = new LotViewModel(LotDao.idLot, LotDao.nomLot, LotDao.descriptionLot, LotDao.liquidationJudiciaire);
            return to;
        }
        public static LotViewModel getLot(string nomLot)
        {
            LotDAO LotDao = LotDAO.GetLot(nomLot);
            LotViewModel to = new LotViewModel(LotDao.idLot, LotDao.nomLot, LotDao.descriptionLot, LotDao.liquidationJudiciaire);
            return to;
        }

        public static ObservableCollection<LotViewModel> listeLot()
        {
            ObservableCollection<LotDAO> lotDao = LotDAO.listeLot();
            ObservableCollection<LotViewModel> toVM = new ObservableCollection<LotViewModel>();
            foreach (LotDAO e in lotDao)
            {
                LotViewModel LotViewModel = new LotViewModel(e.idLot, e.nomLot, e.descriptionLot,  e.liquidationJudiciaire);
                toVM.Add(LotViewModel);

            }

            return toVM;
        }

        public static void updateLot(LotViewModel Lot)
        {
            LotDAO.updateLot(new LotDAO(Lot.idLotProperty, Lot.nomLotProperty, Lot.descriptionLotProperty,  Lot.liquidationJudiciareProperty));
        }

        public static void supprimerLot(int id)
        {
            LotDAO.supprimerLot(id);
        }
        public static void insertLot(LotViewModel Lot)
        {
            LotDAO.insertLot(new LotDAO(Lot.idLotProperty, Lot.nomLotProperty, Lot.descriptionLotProperty, Lot.liquidationJudiciareProperty));
        }
    }
}
