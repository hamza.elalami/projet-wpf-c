﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;


namespace projet
{
    public class UserViewModel : INotifyPropertyChanged
    {
        private int idUser;
        private string nomUser;
        private string prenomUser;
        private string concat = "Ajouter ";
        private string DateDeNaiss;
        private int NumérodeTel;
        private string AdresseMail;

        private AdresseViewModel adresseUser;
        private int type;

        public UserViewModel()
        {
        }

        public UserViewModel(int idUser, string nom, string prenom,string AdresseMail , int NumérodeTel , int type, string DateDeNaiss, AdresseViewModel adresse)

        {
            this.idUser = idUser;
            this.nomUser = nom;
            this.prenomUser = prenom;
            this.DateDeNaiss = DateDeNaiss;
            this.NumérodeTel = NumérodeTel;
            this.AdresseMail = AdresseMail;
            this.adresseUser = adresse;
            this.type = type;

            this.identifiant = nom + "  " + prenom;


        }

        public int idUserProperty
        {
            get { return idUser; }
            set
            {
                idUser = value;
            }
        }

        public string nomUserProperty
        {
            get { return nomUser; }
            set
            {
                nomUser = value.ToUpper();
                ConcatProperty = "Ajouter " + nomUser + " " + prenomUser;
                OnPropertyChanged("nomUserProperty"); 
            }

        }
        public string prenomUserProperty
        {
            get { return prenomUser; }
            set
            {
                this.prenomUser = value;
                ConcatProperty = "Ajouter " + nomUser + " " + prenomUser;
                //OnPropertyChanged("prenomUserProperty");
            }
        }
        private string identifiant;

        public string identifiantProperty
        {
            get { return identifiant; }
            set { identifiant = value; }
        }

        public string DateDeNaissUserProperty
        {
            get { return DateDeNaiss; }
            set
            {
                this.DateDeNaiss = value;
                //OnPropertyChanged(" DateDeNaissUserProperty");
            }
        }
        public int NuméroDeTelUserProperty
        {
            get { return NumérodeTel; }
            set
            {
                this.NumérodeTel = value;
                //nPropertyChanged("NumérodeTelUserProperty");
            }
        }
        public int TypeUserProperty
        {
            get { return type; }
            set
            {
                this.type = value;
            //    OnPropertyChanged("typeUserProperty");
            }
        }
        public string AdresseMailUserProperty
        {
            get { return AdresseMail;}
            set
            {
                this.AdresseMail = value;
              //  OnPropertyChanged("AdresseMailUserProperty");
            }
        }



        public AdresseViewModel adresseUserProperty
        {
            get { return adresseUser; }
            set { adresseUser=  value; }
        }

        

        public string ConcatProperty
        {
            get { return concat; }
            set
            {
                this.concat = value;
                //OnPropertyChanged("ConcatProperty");
            }
        }
        

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));

                    UserORM.updateUser(this);
                
            }


        }

        }
}
