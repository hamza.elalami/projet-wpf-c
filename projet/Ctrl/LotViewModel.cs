﻿using System;
using System.Collections.Generic;
using System.Text;

namespace projet
{
    public class LotViewModel
    {
        private int idLot;

        public int idLotProperty
        {
            get { return idLot; }
            set { idLot = value; }
        }

        private string nomLot;

        public string nomLotProperty
        {
            get { return nomLot; }
            set
            {
                nomLot = value;
                LotORM.updateLot(this);
            }
        }

        private string descriptionLot;

        public string descriptionLotProperty
        {
            get { return descriptionLot; }
            set
            {
                descriptionLot = value;
                LotORM.updateLot(this);

            }
        }


        private int liquidationJudiciaire;

        public int liquidationJudiciareProperty
        {
            get { return liquidationJudiciaire; }
            set { liquidationJudiciaire = value; }
        }

        public LotViewModel() { }

        public LotViewModel(int id, string nom, string description, int liquidationJudiciaire)
        {
            this.idLot = id;
            this.nomLot = nom;
            this.descriptionLot = description;
            this.liquidationJudiciaire = liquidationJudiciaire;
        }


    }
}
