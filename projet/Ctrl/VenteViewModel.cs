﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    public class VenteViewModel : INotifyPropertyChanged
    {
        private int idVente;
        private string date;

        private LotViewModel lotVente;
        private UserViewModel cpVente;
        private LieuEnchereViewModel lieuVente;

        public VenteViewModel()
        {
        }

        public VenteViewModel(int idVente, string date, LotViewModel lot, UserViewModel cp, LieuEnchereViewModel lieu)
        {
            this.idVente = idVente;
            this.date = date;
            this.lotVente = lot;
            this.cpVente = cp;
            this.lieuVente = lieu;
        }

        public int idVenteProperty
        {
            get { return idVente; }
            set 
            {
                idVente = value;
                OnPropertyChanged("idVenteProperty");

            }
        }
        public string dateVenteProperty
        {
            get { return date; }
            set
            {
                date = value;
                OnPropertyChanged("dateVenteProperty");

            }
        }

        public LotViewModel lotVenteProperty
        {
            get { return lotVente; }
            set { lotVente = value; }
        }

        public UserViewModel cpVenteProperty
        {
            get { return cpVente; }
            set { cpVente = value; }
        }

        public LieuEnchereViewModel lieuVenteProperty
        {
            get { return lieuVente; }
            set { lieuVente = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));


                if ((MainWindow.onglet != "ajouter"))
                {


                   // VenteORM.updateVente(this);
                }
            }
        }

    }
}
