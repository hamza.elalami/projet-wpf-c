﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace projet
{
    class EnchereViewModel
    {
        private int idEnchere;
        private double enchere;

        private UserViewModel userEnchere;

        public UserViewModel userEnchereProperty
        {
            get { return userEnchere; }
            set { userEnchere = value; }
        }

        private ObjetViewModel objetEnchere;

        public ObjetViewModel objetEnchereProperty
        {
            get { return objetEnchere; }
            set { objetEnchere = value; }
        }

        public EnchereViewModel()
        {}

        public EnchereViewModel(int idenchere, double enchere, UserViewModel user, ObjetViewModel objet)
        {
            this.idEnchere = idenchere;
            this.enchere = enchere;
            this.userEnchere = user;
            this.objetEnchere = objet;
        }

        public int idEnchereProperty
        {
            get { return this.idEnchere; }
            set { idEnchere = value; }
        }
        public double enchereProperty { 
            get { return this.enchere; }
            set { this.enchere = value; }
        }

        private string nomObjetEnchere;

        public string nomObjetEnchereProperty
        {
            get { return nomObjetEnchere; }
            set { nomObjetEnchere = value; }
        }




    }
}
