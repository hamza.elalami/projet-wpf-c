﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace projet
{
    class TypeObjetViewModel
    {
        private int idTypeObjet;

        public int idTOProperty
        {
            get { return idTypeObjet; }
            set { idTypeObjet = value; }
        }

        private string typeObjet;

        public string typeObjetProperty
        {
            get { return typeObjet; }
            set
            {
                typeObjet = value;
                TypeObjetORM.updateTypeObjet(this);
            }
        }

        public TypeObjetViewModel(){}

        public TypeObjetViewModel(int idTO, string type)
        {
            this.idTypeObjet = idTO;
            this.typeObjet = type;
        }
       
        
    }
}
