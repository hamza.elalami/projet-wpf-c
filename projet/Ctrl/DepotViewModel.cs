﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;


namespace projet
{
    public class DepotViewModel : INotifyPropertyChanged
    {
        private int idDepot;
        private string nomDepot;
        private AdresseViewModel adresseDepot;

        public DepotViewModel()
        {
        }

        public DepotViewModel(int idDepot, string nom, AdresseViewModel adresse)
        {
            this.idDepot = idDepot;
            this.nomDepot = nom;
            this.adresseDepot = adresse;
        }

        public int idDepotProperty
        {
            get { return idDepot; }
            set
            {
                idDepot = value;
            }
        }
        public string nomDepotProperty
        {
            get { return nomDepot; }
            set { nomDepot = value; }
        }

        public AdresseViewModel adresseDepotProperty
        {
            get { return adresseDepot; }
            set { adresseDepot = value; }
        }




        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));

                
                if ((MainWindow.onglet != "ajouter"))
                {

                   
                    DepotORM.updateDepot(this);
                }
            }


        }

        }
}