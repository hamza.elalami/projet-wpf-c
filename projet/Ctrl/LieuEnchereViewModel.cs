﻿
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;


namespace projet
{
    public class LieuEnchereViewModel //: INotifyPropertyChanged
    {
        private int idLieuEnchere;
        private string nomLieuEnchere;
        //private string concatLieuEnchere = "Ajouter ";
        private AdresseViewModel adresseLieuEnchere;

        public LieuEnchereViewModel()
        {
        }

        public LieuEnchereViewModel(int idLieuEnchere, string nom, AdresseViewModel adresse)

        {
            this.idLieuEnchere = idLieuEnchere;
            this.nomLieuEnchere = nom;
            this.adresseLieuEnchere = adresse;
        }
        public int idLieuEnchereProperty
        {
            get { return idLieuEnchere; }
            set
            {
                idLieuEnchere = value;
            }
        }
        
        public string nomLieuEnchereProperty
        {
            get { return nomLieuEnchere; }
            set
            {
                nomLieuEnchere = value.ToUpper();
               // OnPropertyChanged("nomLieuEnchereProperty"); 
            }

        }

        public AdresseViewModel adresseLieuEnchereProperty
        {
            get { return adresseLieuEnchere; }
            set { adresseLieuEnchere = value; }
        }

        /*

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));

                
                if ((MainWindow.onglet != "ajouter"))
                {

                   
                    LieuEnchereORM.updateLieuEnchere(this);
                }
            }


        }*/

        }
}