﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace projet
{
    class ObjetViewModel
    {
        //private int securid;
        //private string securNom;
        private int idObjet;

        public int idObjetProperty
        {
            get { return idObjet; }
            set { idObjet = value; }
        }

        private string nomObjet;

        public string nomObjetProperty
        {
            get { return nomObjet; }
            set
            {
                nomObjet = value;
            }
        }

        private string artisteObjet;

        public string artisteObjetProperty
        {
            get { return artisteObjet; }
            set
            {
                artisteObjet = value;

            }
        }

        private double prixDepart;

        public double prixDepartProperty
        {
            get { return prixDepart; }
            set
            {
                prixDepart = value;

            }
        }

        private double prixFin;

        public double prixfinProperty
        {
            get { return prixFin; }
            set
            {
                prixFin = value;

            }
        }

        private double prixFlash;

        public double prixFlashProperty
        {
            get { return prixFlash; }
            set
            {
                prixFlash = value;

            }
        }

        private double prixMinEnchere;

        public double prixMinEnchereProperty
        {
            get { return prixMinEnchere; }
            set
            {
                prixMinEnchere = value;

            }
        }

        private int vendu;

        public int venduProperty
        {
            get { return vendu; }
            set { vendu = value; }
        }

        private string phraseVendu;

        public string phraseVenduProperty
        {
            get { return phraseVendu; }
            set
            {
                if (value == "Vendu")
                {
                    phraseVendu = value;
                    venduProperty = 1;
                }
                else if (value == "A vendre")
                {
                    phraseVendu = value;
                    venduProperty = 0;
                }
                else
                {
                    MessageBox.Show("Terme invalide (Vendu / A vendre");
                }
            }
        }

        private TypeObjetViewModel typeObjet;

        public TypeObjetViewModel typeObjetProperty
        {
            get { return typeObjet; }
            set { typeObjet = value; }
        }
        /*public string nomTypeObjetProperty1
        {
            get { return nomTypeObjet; }
            set
            {
                securNom = nomTypeObjet;
                securid = id_TypeObjet_Objet;
                try
                {
                    nomTypeObjet = value;
                    id_TypeObjet_Objet = ObjetDAL.getIdTypeObjet(value);
                    ObjetORM.updateObjet(this);
                }
                catch (Exception e)
                {
                    nomTypeObjet = securNom;
                    id_TypeObjet_Objet = securid;
                    MessageBox.Show("Utilisateur existants" + e);
                }

            }
        }*/

        private LotViewModel lotObjet;

        public LotViewModel lotObjetProperty
        {
            get { return lotObjet; }
            set { lotObjet = value; }
        }

        /*
        public string nomLotObjetProperty1
        {
            get { return nomLotObjet; }
            set
            {
                securNom = nomLotObjet;
                securid = id_Lot_Objet;
                
                try
                {
                    nomLotObjet = value;
                    id_Lot_Objet = ObjetDAL.getIdLot(value);
                    ObjetORM.updateObjet(this);
                }
                catch (Exception e)
                {
                    nomLotObjet = securNom;
                    id_Lot_Objet = securid;
                    MessageBox.Show("Le nom du lot est incorrecte, vérifié les lots existants" + e);
                }
            }
        }
        */

        private UserViewModel vendeurObjet;
        public UserViewModel vendeurObjetProperty
        {
            get { return vendeurObjet; }
            set { vendeurObjet = value; }
        }
        /*private string nomPrenomVendeur;

        public string nomPrenomVendeurProperty
        {
            get { return nomPrenomVendeur; }
            set
            {
                securNom = nomPrenomVendeur;
                securid = id_Vendeur_Objet;
                try
                {
                    nomPrenomVendeur = value;
                    id_Vendeur_Objet = ObjetDAL.getIdUtilisateur(value);
                    ObjetORM.updateObjet(this);
                }
                catch (Exception e)
                {
                    nomPrenomVendeur = securNom;
                    id_Vendeur_Objet = securid;
                    MessageBox.Show("Utilisateur existants" + e);
                }
            }
        }
        */

        private UserViewModel acheteurObjet;

        public UserViewModel acheteurObjetProperty
        {
            get { return acheteurObjet; }
            set { acheteurObjet = value; }
        }
        /*
        public string nomPrenomAcheteurProperty
        {
            get { return nomPrenomAcheteur; }
            set 
            {
                securNom = nomPrenomAcheteur;
                securid = id_Acheteur_Objet;
                try
                {
                    nomPrenomAcheteur = value;
                    id_Acheteur_Objet = ObjetDAL.getIdUtilisateur(value);
                    ObjetORM.updateObjet(this);
                }
                catch (Exception e)
                {
                    nomPrenomAcheteur = securNom;
                    id_Acheteur_Objet = securid;
                    MessageBox.Show("Utilisateur inexistants" + e);
                }
            }
        }
        */
        private DepotViewModel depotObjet;

        public DepotViewModel depotObjetProperty
        {
            get { return depotObjet; }
            set { depotObjet = value; }
        }
        /*
        public string nomDepotProperty1
        {
            get { return nomDepotObjet; }
            set 
            {
                securNom = nomDepotObjet;
                securid = id_Depot_Objet;
                try
                {
                    nomDepotObjet = value;
                    id_Depot_Objet = ObjetDAL.getIdDepot(value);
                    ObjetORM.updateObjet(this);
                }
                catch (Exception e)
                {
                    nomDepotObjet = securNom;
                    id_Depot_Objet = securid;
                    MessageBox.Show("Depot inexistants" + e);
                }
            }
        }*/
        

        public ObjetViewModel()
        {
        }

        public ObjetViewModel(int idObjet, string nom, string artiste, double prixdepart, double prixfin, double prixflash, double prixmin, int vendu, TypeObjetViewModel type, LotViewModel lot, UserViewModel vendeur, UserViewModel acheteur, DepotViewModel depot)
        {
            this.idObjet = idObjet;
            this.nomObjet = nom;
            this.artisteObjet = artiste;
            this.prixDepart = prixdepart;
            this.prixFin = prixfin;
            this.prixFlash = prixflash;
            this.prixMinEnchere = prixmin;
            this.vendu = vendu;
            switch (vendu)
            {
                case 0:
                    this.phraseVendu = "A vendre";
                    break;
                case 1:
                    this.phraseVendu = "Vendu";
                    break;
            }

            this.typeObjet = type;
            this.lotObjet = lot;
            this.vendeurObjet = vendeur;
            if( acheteur != null)
                this.acheteurObjet = acheteur;
            this.depotObjet = depot;
        }
    }
}
