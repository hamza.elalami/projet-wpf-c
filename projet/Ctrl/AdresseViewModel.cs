﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace projet
{
    public class AdresseViewModel : INotifyPropertyChanged
    {
        private int idadresse;
        private string region;
        private string departement;
        private string ville;
        private string rue;
        private int num;
        private string adresseComplete;

        public AdresseViewModel()
        {

        }

        public AdresseViewModel(int idadresse, string region, string departement, string ville, string rue, int num)
        {
            this.idadresse = idadresse;
            this.region = region;
            this.departement = departement;
            this.ville = ville;
            this.rue = rue;
            this.num = num;
            this.adresseComplete = region + departement + ville + rue + num;
        }

        public int idAdresseProperty
        {
            get { return idadresse; }
            set
            {
                idadresse = value;
            }
        }
        public string regionProperty
        {
            get { return region; }
            set
            {
                this.region = value;
                OnPropertyChanged("RegionProperty");

            }
        }

        public string departementProperty
        {
            get { return departement; }
            set
            {
                departement = value;
                OnPropertyChanged("DepartementProperty");
            }
        }

        public string villeProperty
        {
            get { return ville; }
            set
            {
                ville = value;
                OnPropertyChanged("VilleProperty");
            }
        }


        public string rueProperty
        {
            get { return rue; }
            set
            {
                rue = value;
                OnPropertyChanged("RueProperty");
            }
        }

        public int numProperty
        {
            get { return num; }
            set
            {
                num = value;
                OnPropertyChanged("NumProperty");
            }
        }

        public string adresseCompleteProperty
        {
            get { return adresseComplete; }
            set { adresseComplete = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));

                /*
                if ((MainWindow.onglet != "ajouter"))
                {


                }*/
            }


        }

    }
}
