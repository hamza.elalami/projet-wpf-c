﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class OrdreAchatViewModel
    {
        private double prixMax;

        public double prixMaxProperty
        {
            get { return prixMax; }
            set { prixMax = value; }
        }

        private UserViewModel userOA;

        public UserViewModel userOAProperty
        {
            get { return userOA; }
            set { userOA = value; }
        }

        private ObjetViewModel objetOA;

        public ObjetViewModel objetOAProperty
        {
            get { return objetOA; }
            set { objetOA = value; }
        }

        
        public OrdreAchatViewModel(){}

        public OrdreAchatViewModel(double prix, UserViewModel user, ObjetViewModel objet)
        {
            this.prixMax = prix;
            this.userOA = user;
            this.objetOA = objet;
        }
    }
}
