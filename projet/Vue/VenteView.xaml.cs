﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace projet
{
    /// <summary>
    /// Logique d'interaction pour VenteView.xaml
    /// </summary>
    public partial class VenteView : UserControl
    {
        int selectedVenteId;

        VenteViewModel myDataObjectVente;
        
        ObservableCollection<VenteViewModel> oVente;
        ObservableCollection<LieuEnchereViewModel> oLieu;
        ObservableCollection<UserViewModel> oUser;
        ObservableCollection<LotViewModel> oLot;

        public VenteView()
        {
            InitializeComponent();
            loadEnchere();

        }

        void loadEnchere()
        {
            oVente = VenteORM.listeVente();
            oUser = UserORM.listeUsers();
            oLot = LotORM.listeLot();
            oLieu = LieuEnchereORM.listeLieuEnchere();
            myDataObjectVente = new VenteViewModel();
            //LIEN AVEC la VIEW
            listeEnchere.ItemsSource = oVente; // bind de la liste avec la source, permettant le binding.
            LotTxxB.TextChanged += new TextChangedEventHandler(lotObjet_TextChanged);
            CPTxxB.TextChanged += new TextChangedEventHandler(Uti_TextChanged);
            LieuEnchereTxxB.TextChanged += new TextChangedEventHandler(LieuEnchere_TextChanged);
            lbSuggestLieuEnchere.Visibility = Visibility.Collapsed;
            lbSuggestUTI.Visibility = Visibility.Collapsed;
            lbSuggestLot.Visibility = Visibility.Collapsed;
        }
        private void EnchereButton_Click(object sender, RoutedEventArgs e)
        {
            myDataObjectVente.idVenteProperty = VenteDAL.getMaxIdVente() + 1;
            myDataObjectVente.dateVenteProperty = DateenchereTxxB.Text;

            myDataObjectVente.lotVenteProperty = LotORM.getLot(LotTxxB.Text);
            //myDataObjectVente.lotVenteProperty.nomLotProperty = LotTxxB.Text;

            myDataObjectVente.cpVenteProperty = UserORM.getUser(CPTxxB.Text);
            //myDataObjectVente.cpVenteProperty.nomUserProperty = CPTxxB.Text;

            myDataObjectVente.lieuVenteProperty = LieuEnchereORM.getLieuEnchere(LieuEnchereTxxB.Text);
            //myDataObjectVente.lieuVenteProperty.nomLieuEnchereProperty = LieuEnchereTxxB.Text;

            /*string[] /*adresse = LieuEnchereTxxB.Text.Split("  ");
            myDataObjectVente.lieuVenteProperty.nomLieuEnchereProperty = adresse[0];
            myDataObjectVente.lieuVenteProperty.adresseLieuEnchereProperty.regionProperty = adresse[1];
            myDataObjectVente.lieuVenteProperty.adresseLieuEnchereProperty.departementProperty = adresse[2];
            myDataObjectVente.lieuVenteProperty.adresseLieuEnchereProperty.villeProperty = adresse[3];
            myDataObjectVente.lieuVenteProperty.adresseLieuEnchereProperty.rueProperty = adresse[4];
            myDataObjectVente.lieuVenteProperty.adresseLieuEnchereProperty.numProperty = int.Parse(adresse[5]);*/

            oVente.Add(myDataObjectVente);
            VenteORM.insertVente(myDataObjectVente);

            listeEnchere.Items.Refresh();

            myDataObjectVente = new VenteViewModel();

            DateenchereTxxB.DataContext = myDataObjectVente;
            LotTxxB.DataContext = myDataObjectVente;
            CPTxxB.DataContext = myDataObjectVente;
            LieuEnchereTxxB.DataContext = myDataObjectVente;
            nomDpDpButton1.DataContext = myDataObjectVente;
            lbSuggestUTI.Items.Refresh();
            lbSuggestLieuEnchere.Items.Refresh();

        }
        private void supprimerButtonEnchere_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (listeEnchere.SelectedItem is VenteViewModel)
            {
                try
                {
                    VenteORM.supprimerVente(selectedVenteId);
                    VenteViewModel toRemove = (VenteViewModel)listeEnchere.SelectedItem;
                    oVente.Remove(toRemove);
                    listeEnchere.Items.Refresh();

                }
                catch (Exception exception)
                {
                    MessageBox.Show("Vous ne pouvez pas supprimer cette vente. abruti." + exception);
                }
                
            }
        }
        private void listeEnchere_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((listeEnchere.SelectedIndex < oVente.Count) && (listeEnchere.SelectedIndex >= 0))
            {
                selectedVenteId = (oVente.ElementAt<VenteViewModel>(listeEnchere.SelectedIndex)).idVenteProperty;
            }
        }
        private void DepotButton_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }
        public void Uti_TextChanged(object sender, TextChangedEventArgs el)
        {
            string typeString = CPTxxB.Text;
            List<string> autoList = new List<string>();
            autoList.Clear();

            foreach (UserViewModel mydataobject in oUser)
            {


                if (!string.IsNullOrEmpty(CPTxxB.Text))
                {
                    if (mydataobject.identifiantProperty.StartsWith(typeString) && (mydataobject.TypeUserProperty >=5 || mydataobject.TypeUserProperty == 3))
                    {
                        autoList.Add(mydataobject.identifiantProperty);
                    }
                    else if (mydataobject.prenomUserProperty.StartsWith(typeString) && mydataobject.TypeUserProperty >= 10)
                    {
                        autoList.Add(mydataobject.identifiantProperty);
                    }
                }
            }

            if (autoList.Count > 0)
            {
                lbSuggestUTI.ItemsSource = autoList;
                lbSuggestUTI.Visibility = Visibility.Visible;
            }
            else if (CPTxxB.Text.Equals(""))
            {
                lbSuggestUTI.Visibility = Visibility.Collapsed;
                lbSuggestUTI.ItemsSource = null;
            }
            else
            {
                lbSuggestUTI.Visibility = Visibility.Collapsed;
                lbSuggestUTI.ItemsSource = null;
            }

        }
        private void lbSuggesttion_selectionChangedUTI(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestUTI.ItemsSource != null)
            {
                lbSuggestUTI.Visibility = Visibility.Collapsed;
                CPTxxB.TextChanged -= new TextChangedEventHandler(Uti_TextChanged);
                if (lbSuggestUTI.SelectedIndex != -1)
                {
                    CPTxxB.Text = lbSuggestUTI.SelectedItems[0].ToString();
                }
                CPTxxB.TextChanged += new TextChangedEventHandler(Uti_TextChanged);
            }
        }
        public void LieuEnchere_TextChanged(object sender, TextChangedEventArgs el)
        {
            string typeString = LieuEnchereTxxB.Text;
            List<string> autoList = new List<string>();
            autoList.Clear();

            foreach (LieuEnchereViewModel mydataobjectlieuenchere in oLieu)
            {
                if (!string.IsNullOrEmpty(LieuEnchereTxxB.Text))
                {
                    if(mydataobjectlieuenchere.nomLieuEnchereProperty.StartsWith(typeString.ToUpper()) )
                    {
                        autoList.Add(mydataobjectlieuenchere.nomLieuEnchereProperty);
                    }
                    /*if (mydataobjectlieuenchere.adresseLieuEnchereProperty.departementProperty.StartsWith(typeString))
                    {
                        autoList.Add(mydataobjectlieuenchere.adresseLieuEnchereProperty.adresseCompleteProperty);
                    }
                    else if (mydataobjectlieuenchere.adresseLieuEnchereProperty.regionProperty.StartsWith(typeString))
                    {
                        autoList.Add((mydataobjectlieuenchere.adresseLieuEnchereProperty.adresseCompleteProperty));
                    }
                    else if(mydataobjectlieuenchere.adresseLieuEnchereProperty.villeProperty.StartsWith(typeString))
                    {
                        autoList.Add((mydataobjectlieuenchere.adresseLieuEnchereProperty.adresseCompleteProperty));
                    }
                    else if (mydataobjectlieuenchere.nomLieuEnchereProperty.StartsWith(typeString))
                    {
                        autoList.Add((mydataobjectlieuenchere.adresseLieuEnchereProperty.adresseCompleteProperty));
                    }*/
                }
            }

            if (autoList.Count > 0)
            {
                lbSuggestLieuEnchere.ItemsSource = autoList;
                lbSuggestLieuEnchere.Visibility = Visibility.Visible;
            }
            else if (CPTxxB.Text.Equals(""))
            {
                lbSuggestLieuEnchere.Visibility = Visibility.Collapsed;
                lbSuggestLieuEnchere.ItemsSource = null;
            }
            else
            {
                lbSuggestLieuEnchere.Visibility = Visibility.Collapsed;
                lbSuggestLieuEnchere.ItemsSource = null;
            }

        }
        private void lbSuggesttion_selectionChangedLieuEnchere(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestLieuEnchere.ItemsSource != null)
            {
                lbSuggestLieuEnchere.Visibility = Visibility.Collapsed;
                LieuEnchereTxxB.TextChanged -= new TextChangedEventHandler(LieuEnchere_TextChanged);
                if (lbSuggestLieuEnchere.SelectedIndex != -1)
                {
                    LieuEnchereTxxB.Text = lbSuggestLieuEnchere.SelectedItems[0].ToString();
                }
                LieuEnchereTxxB.TextChanged += new TextChangedEventHandler(LieuEnchere_TextChanged);
            }
        }
        public void lotObjet_TextChanged(object sender, TextChangedEventArgs el)
        {
            string typeString = LotTxxB.Text;
            List<string> autoList = new List<string>();
            autoList.Clear();
            foreach (LotViewModel olot in oLot)
            {
                if (!string.IsNullOrEmpty(LotTxxB.Text))
                {
                    if (olot.nomLotProperty.StartsWith(typeString))
                    {
                        autoList.Add(olot.nomLotProperty);
                    }
                }
            }

            if (autoList.Count > 0)
            {
                lbSuggestLot.ItemsSource = autoList;
                lbSuggestLot.Visibility = Visibility.Visible;
            }
            else if (LotTxxB.Text.Equals(""))
            {
                lbSuggestLot.Visibility = Visibility.Collapsed;
                lbSuggestLot.ItemsSource = null;
            }
            else
            {
                lbSuggestLot.Visibility = Visibility.Collapsed;
                lbSuggestLot.ItemsSource = null;
            }
        }
        public void lbSuggesttionLot_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestLot.ItemsSource != null)
            {
                lbSuggestLot.Visibility = Visibility.Collapsed;
                LotTxxB.TextChanged -= new TextChangedEventHandler(lotObjet_TextChanged);
                if (lbSuggestLot.SelectedIndex != -1)
                {
                    LotTxxB.Text = lbSuggestLot.SelectedItems[0].ToString();
                }
                LotTxxB.TextChanged += new TextChangedEventHandler(lotObjet_TextChanged);
            }
        }
        
    }
}
