﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace projet
{
    /// <summary>
    /// Logique d'interaction pour DepotView.xaml
    /// </summary>
    public partial class DepotView : UserControl
    {
        //int selectedAdresseId;
        int selectedDepotId; 

        DepotViewModel myDataObjectDepot;
        ObservableCollection<DepotViewModel> liste2;
        public DepotView()
        {
            InitializeComponent();
            loadDepot();
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9-,]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        void loadDepot()
        {
            liste2 = DepotORM.listeDepot();
            myDataObjectDepot = new DepotViewModel();
            //LIEN AVEC la VIEW
            listeDepot.ItemsSource = liste2; // bind de la liste avec la source, permettant le binding.
        }

        private void DepotButton_Click(object sender, RoutedEventArgs e)
        {
            myDataObjectDepot.idDepotProperty = DepotDAL.getMaxIdDepot() + 1;
            myDataObjectDepot.adresseDepotProperty = new AdresseViewModel(AdresseDAL.getMaxIdAdresse() + 1, RegionDepotTextBox.Text, DepartementDepotTextBox.Text, VilleDepotTextBox.Text, RueDepotTextBox.Text,  int.Parse(NumeroDepotTextBox.Text));
            myDataObjectDepot.nomDepotProperty = nomDepotTextBox.Text;

            liste2.Add(myDataObjectDepot);
            AdresseORM.insertAdresse(myDataObjectDepot.adresseDepotProperty);
            DepotORM.insertDepot(myDataObjectDepot);
            listeDepot.Items.Refresh();
            myDataObjectDepot = new DepotViewModel();

            nomDepotTextBox.DataContext = myDataObjectDepot;
            RegionDepotTextBox.DataContext = myDataObjectDepot;
            DepartementDepotTextBox.DataContext = myDataObjectDepot;
            VilleDepotTextBox.DataContext = myDataObjectDepot;
            RueDepotTextBox.DataContext = myDataObjectDepot;
            NumeroDepotTextBox.DataContext = myDataObjectDepot;
            nomDpDpButton.DataContext = myDataObjectDepot;
        }

        private void listeDepot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((listeDepot.SelectedIndex < liste2.Count) && (listeDepot.SelectedIndex >= 0))
            {
                selectedDepotId = (liste2.ElementAt<DepotViewModel>(listeDepot.SelectedIndex)).idDepotProperty;
            }
        }

        private void supprimerButtonDepot_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (listeDepot.SelectedItem is DepotViewModel)
            {
                try
                {
                    int idAdresse = DepotORM.getDepot(selectedDepotId).adresseDepotProperty.idAdresseProperty;
                    DepotORM.supprimerDepot(selectedDepotId);
                    AdresseORM.supprimerAdresse(idAdresse);
                    DepotViewModel toRemove = (DepotViewModel)listeDepot.SelectedItem;
                    liste2.Remove(toRemove);
                    listeDepot.Items.Refresh();
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Vous ne pouvez pas supprimer ce dépot." + exception);
                }
                
                
            }
        }
        private void DepotButton_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }
    }
}
