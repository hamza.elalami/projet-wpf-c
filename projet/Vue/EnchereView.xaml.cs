﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

using System.Windows.Input;


namespace projet
{
    /// <summary>
    /// Logique d'interaction pour EnchereView.xaml
    /// </summary>
    public partial class EnchereView : UserControl
    {
        EnchereViewModel myDataEnchere;
        ObservableCollection<EnchereViewModel> oEnchere;
        ObservableCollection<VenteViewModel> oVente;
        private ObservableCollection<ObjetViewModel> oObjet;
        int lotDuJour;

        public EnchereView()
        {
            InitializeComponent();
            loadEnchere();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9-,]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void loadEnchere()
        {
            oEnchere = EnchereORM.listeEnchere();
            myDataEnchere = new EnchereViewModel();
            listeEnchere.ItemsSource = oEnchere;

            oVente = VenteORM.listeVente();
            oObjet = ObjetORM.listeObjet();
            lbSuggestVente.Visibility = Visibility.Collapsed;
            lbSuggestObjet.Visibility = Visibility.Collapsed;

            ObjetEnchere.TextChanged += new TextChangedEventHandler(VenteActuel_TextChanged);
            VenteActuel.TextChanged += new TextChangedEventHandler(VenteActuel_TextChanged);


        }
        private void enchereButton_Click(object sender, RoutedEventArgs e)
        {
            myDataEnchere.idEnchereProperty = EnchereDAL.getMaxIdEnchere() + 1;
            myDataEnchere.objetEnchereProperty = ObjetORM.getObjet(ObjetEnchere.Text);
            myDataEnchere.nomObjetEnchereProperty = ObjetEnchere.Text;
            myDataEnchere.enchereProperty = int.Parse(derniereEnchere.Text);
            myDataEnchere.userEnchereProperty = UserORM.getUser(int.Parse(encherisseur.Text));
            oEnchere.Add(myDataEnchere);
            EnchereORM.insertEnchere(myDataEnchere);

            listeEnchere.Items.Refresh();
            myDataEnchere = new EnchereViewModel();

            derniereEnchere.DataContext = myDataEnchere;
            encherisseur.DataContext = myDataEnchere;

        }

        public void VenteActuel_TextChanged(object sender, TextChangedEventArgs el)
        {
            string typeString = VenteActuel.Text;
            List<string> autoList = new List<string>();
            autoList.Clear();

            foreach (VenteViewModel mydataobject in oVente)
            {
                if (!string.IsNullOrEmpty(VenteActuel.Text))
                {
                    if (mydataobject.lieuVenteProperty.nomLieuEnchereProperty.StartsWith(typeString) || mydataobject.dateVenteProperty.StartsWith(typeString))
                    {
                        autoList.Add(mydataobject.lieuVenteProperty.nomLieuEnchereProperty + "  " + mydataobject.dateVenteProperty);
                        
                    }
                }
            }
            if (autoList.Count > 0)
            {
                lbSuggestVente.ItemsSource = autoList;
                lbSuggestVente.Visibility = Visibility.Visible;
            }
            else if (VenteActuel.Text.Equals(""))
            {
                lbSuggestVente.Visibility = Visibility.Collapsed;
                lbSuggestVente.ItemsSource = null;
            }
            else
            {
                lbSuggestVente.Visibility = Visibility.Collapsed;
                lbSuggestVente.ItemsSource = null;
            }

        }
        private void lbSuggesttion_selectionChangedVenteActuel(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestVente.ItemsSource != null)
            {
                lbSuggestVente.Visibility = Visibility.Collapsed;
                VenteActuel.TextChanged -= new TextChangedEventHandler(VenteActuel_TextChanged);
                if (lbSuggestVente.SelectedIndex != -1)
                {
                    VenteActuel.Text = lbSuggestVente.SelectedItems[0].ToString();
                    lotDuJour = VenteORM.getVente(VenteActuel.Text).lotVenteProperty.idLotProperty;
                }
                VenteActuel.TextChanged += new TextChangedEventHandler(VenteActuel_TextChanged);
            }
        }

        
        public void Objet_TextChanged(object sender, TextChangedEventArgs el)
        {
            string typeString = ObjetEnchere.Text;
            List<string> autoList = new List<string>();
            autoList.Clear();

            foreach (ObjetViewModel mydataobject in oObjet)
            {
                if (!string.IsNullOrEmpty(ObjetEnchere.Text))
                {
                    if (mydataobject.nomObjetProperty.StartsWith(typeString) && mydataobject.lotObjetProperty.idLotProperty == lotDuJour)
                    {
                        autoList.Add(mydataobject.nomObjetProperty);
                    }
                }
            }

            if (autoList.Count > 0)
            {
                lbSuggestObjet.ItemsSource = autoList;
                lbSuggestObjet.Visibility = Visibility.Visible;
            }
            else if (VenteActuel.Text.Equals(""))
            {
                lbSuggestObjet.Visibility = Visibility.Collapsed;
                lbSuggestObjet.ItemsSource = null;
            }
            else
            {
                lbSuggestObjet.Visibility = Visibility.Collapsed;
                lbSuggestObjet.ItemsSource = null;
            }
        }
        private void lbSuggesttionObjet_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestObjet.ItemsSource != null)
            {
                lbSuggestObjet.Visibility = Visibility.Collapsed;
                ObjetEnchere.TextChanged -= new TextChangedEventHandler(VenteActuel_TextChanged);
                if (lbSuggestObjet.SelectedIndex != -1)
                {
                    ObjetEnchere.Text = lbSuggestObjet.SelectedItems[0].ToString();
                }
                ObjetEnchere.TextChanged += new TextChangedEventHandler(VenteActuel_TextChanged);
            }
        }

    }
}
