﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace projet
{
    /// <summary>
    /// Logique d'interaction pour LieuEnchere.xaml
    /// </summary>
    /// 
    public partial class LieuEnchere : UserControl
    {
        LieuEnchereViewModel myDataObjectLieuEnchere;
        private ObservableCollection<LieuEnchereViewModel> liste22;
        //int selectedDepotId;
        //DepotViewModel myDataObjectDepot;

        int selectedLieuEnchereId;
        //int selectedAdresseId;
        public LieuEnchere()
        {
            InitializeComponent();
            loadLieuEnchere();
        }
        private void LieuEnchereButton_Click(object sender, RoutedEventArgs e)
        {
            myDataObjectLieuEnchere.idLieuEnchereProperty = LieuEnchereDAL.getMaxIdLieuEnchere() + 1;
            myDataObjectLieuEnchere.adresseLieuEnchereProperty = new AdresseViewModel(AdresseDAL.getMaxIdAdresse() + 1, RegionLieuEnchereTxxB.Text, DepartementLieuEnchereTxxB.Text, VilleLieuEnchereTxxB.Text, RueLieuEnchereTextBoxTxxB.Text, int.Parse(NumeroLieuEnchereTxxB.Text));
            myDataObjectLieuEnchere.nomLieuEnchereProperty = nomLieuEnchereTxxB.Text;

            liste22.Add(myDataObjectLieuEnchere);
            AdresseORM.insertAdresse(myDataObjectLieuEnchere.adresseLieuEnchereProperty);
            LieuEnchereORM.insertLieuEnchere(myDataObjectLieuEnchere);
            listeLieuEnchere.Items.Refresh();
            myDataObjectLieuEnchere = new LieuEnchereViewModel();

            nomLieuEnchereTxxB.DataContext = myDataObjectLieuEnchere;
            RegionLieuEnchereTxxB.DataContext = myDataObjectLieuEnchere;
            DepartementLieuEnchereTxxB.DataContext = myDataObjectLieuEnchere;
            VilleLieuEnchereTxxB.DataContext = myDataObjectLieuEnchere;
            RueLieuEnchereTextBoxTxxB.DataContext = myDataObjectLieuEnchere;
            NumeroLieuEnchereTxxB.DataContext = myDataObjectLieuEnchere;
            nomDpDpButton22.DataContext = myDataObjectLieuEnchere;
        }
        public void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9-,]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void supprimerButtonLieuEnchere_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (listeLieuEnchere.SelectedItem is LieuEnchereViewModel)
            {
                try
                {
                    int idAdresse = DepotORM.getDepot(selectedLieuEnchereId).adresseDepotProperty.idAdresseProperty;
                    LieuEnchereORM.supprimerLieuEnchere(selectedLieuEnchereId);
                    AdresseORM.supprimerAdresse(idAdresse);
                    LieuEnchereViewModel toRemove = (LieuEnchereViewModel)listeLieuEnchere.SelectedItem;
                    liste22.Remove(toRemove);
                    listeLieuEnchere.Items.Refresh();
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Vous ne pouvez pas supprimer le lieu de vente."+ exception);
                }
            }
        }

        private void DepotButton_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }
        private void listeLieuEnchere_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((listeLieuEnchere.SelectedIndex < liste22.Count) && (listeLieuEnchere.SelectedIndex >= 0))
            {
                selectedLieuEnchereId = (liste22.ElementAt<LieuEnchereViewModel>(listeLieuEnchere.SelectedIndex)).idLieuEnchereProperty;
            }
        }

        void loadLieuEnchere()
        {
            liste22 = LieuEnchereORM.listeLieuEnchere();
            myDataObjectLieuEnchere = new LieuEnchereViewModel();
            //LIEN AVEC la VIEW
            listeLieuEnchere.ItemsSource = liste22; // bind de la liste avec la source, permettant le binding.
        }

    }
}
