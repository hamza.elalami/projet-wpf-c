﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

using System.Windows.Input;

namespace projet
{
    /// <summary>
    /// Logique d'interaction pour UserControl2.xaml
    /// </summary>
    public partial class ObjetView : UserControl
    {


        //TypeObjetViewModel myDataTypeObjet;
        //private LotViewModel myDataLot;
        private ObjetViewModel MyDataObjet;
        //private OrdreAchatViewModel MyDataOrdreAchat;


        private ObservableCollection<DepotViewModel> oDepot;
        //private ObservableCollection<OrdreAchatViewModel> oOrdreAchat;
        private ObservableCollection<ObjetViewModel> oObjet;
        private ObservableCollection<LotViewModel> oLot;
        private ObservableCollection<TypeObjetViewModel> oTypeObjet;
        private ObservableCollection<UserViewModel> oUser;

        private int idObjet;
        public ObjetView()
        {
            InitializeComponent();
            loadObjet();
            lbSuggestLot.Items.Refresh();
            lbSuggestType.Items.Refresh();
            lbSuggestUtilisateur.Items.Refresh();
            lbSuggestDepot.Items.Refresh();

        }

        private void loadObjet()
        {
            oTypeObjet = TypeObjetORM.listeTypeObjet();
            //myDataTypeObjet = new TypeObjetViewModel();
            //listeTypeObjet.ItemsSource = oTypeObjet;

            oDepot = DepotORM.listeDepot();
            //myDataDepot = new DepotViewModel();

            oLot = LotORM.listeLot();
            //myDataLot = new LotViewModel();
            //listeLot.ItemsSource = oLot;

            oUser = UserORM.listeUsers();
            //myDataUser = new UserViewModel();

            oObjet = ObjetORM.listeObjet();

            MyDataObjet = new ObjetViewModel();
            listeObjet.ItemsSource = oObjet;

            lotObjet.TextChanged += new TextChangedEventHandler(lotObjet_TextChanged);
            utilisateurObjet.TextChanged += new TextChangedEventHandler(utilisateurObjet_TextChanged);
            typeObjetObjet.TextChanged += new TextChangedEventHandler(typeObjetObjet_TextChanged);
            depotObjet.TextChanged += new TextChangedEventHandler(depotObjet_TextChanged);
            lbSuggestDepot.Visibility = Visibility.Collapsed;
            lbSuggestLot.Visibility = Visibility.Collapsed;
            lbSuggestType.Visibility = Visibility.Collapsed;
            lbSuggestUtilisateur.Visibility = Visibility.Collapsed;
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9-,]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void lotObjet_TextChanged(object sender, TextChangedEventArgs el)
        {
            string typeString = lotObjet.Text;
            List<string> autoList = new List<string>();
            autoList.Clear();
            foreach (LotViewModel e in oLot)
            {
                if (!string.IsNullOrEmpty(lotObjet.Text))
                {
                    if (e.nomLotProperty.StartsWith(typeString))
                    {
                        autoList.Add(e.nomLotProperty);
                    }
                }
            }

            if (autoList.Count > 0)
            {
                lbSuggestLot.ItemsSource = autoList;
                lbSuggestLot.Visibility = Visibility.Visible;
            }
            else if (lotObjet.Text.Equals(""))
            {
                lbSuggestLot.Visibility = Visibility.Collapsed;
                lbSuggestLot.ItemsSource = null;
            }
            else
            {
                lbSuggestLot.Visibility = Visibility.Collapsed;
                lbSuggestLot.ItemsSource = null;
            }
        }

        private void lbSuggesttionLot_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestLot.ItemsSource != null)
            {
                lbSuggestLot.Visibility = Visibility.Collapsed;
                lotObjet.TextChanged -= new TextChangedEventHandler(lotObjet_TextChanged);
                if (lbSuggestLot.SelectedIndex != -1)
                {
                    lotObjet.Text = lbSuggestLot.SelectedItems[0].ToString();
                }
                lotObjet.TextChanged += new TextChangedEventHandler(lotObjet_TextChanged);
            }
        }
        private void utilisateurObjet_TextChanged(object sender, TextChangedEventArgs el)
        {
            string typeString = utilisateurObjet.Text;
            List<string> autoList = new List<string>();
            autoList.Clear();
            foreach (UserViewModel e in oUser)
            {
                if (!string.IsNullOrEmpty(utilisateurObjet.Text) && (e.TypeUserProperty == 2 || e.TypeUserProperty == 4 || e.TypeUserProperty == 6 || e.TypeUserProperty == 7) )
                {
                    if (e.nomUserProperty.StartsWith(typeString) )
                    {
                        autoList.Add(e.identifiantProperty);
                    }
                    else if (e.prenomUserProperty.StartsWith(typeString) )
                    {
                        autoList.Add((e.identifiantProperty));
                    }
                }
            }
            if (autoList.Count > 0)
            {
                lbSuggestUtilisateur.ItemsSource = autoList;
                lbSuggestUtilisateur.Visibility = Visibility.Visible;
            }
            else if (utilisateurObjet.Text.Equals(""))
            {
                lbSuggestUtilisateur.Visibility = Visibility.Collapsed;
                lbSuggestUtilisateur.ItemsSource = null;
            }
            else
            {
                lbSuggestUtilisateur.Visibility = Visibility.Collapsed;
                lbSuggestUtilisateur.ItemsSource = null;
            }


        }

        private void lbSuggesttionUtilisateur_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestUtilisateur.ItemsSource != null)
            {
                lbSuggestUtilisateur.Visibility = Visibility.Collapsed;
                utilisateurObjet.TextChanged -= new TextChangedEventHandler(utilisateurObjet_TextChanged);
                if (lbSuggestUtilisateur.SelectedIndex != -1)
                {
                    utilisateurObjet.Text = lbSuggestUtilisateur.SelectedItems[0].ToString();
                }
                utilisateurObjet.TextChanged += new TextChangedEventHandler(utilisateurObjet_TextChanged);
            }
        }

        private void typeObjetObjet_TextChanged(object sender, TextChangedEventArgs el)
        {
            string typeString = typeObjetObjet.Text;
            List<string> autoList = new List<string>();
            autoList.Clear();
            foreach (TypeObjetViewModel e in oTypeObjet)
            {
                if (!string.IsNullOrEmpty(typeObjetObjet.Text))
                {
                    if (e.typeObjetProperty.StartsWith(typeString))
                    {
                        autoList.Add(e.typeObjetProperty);
                    }
                }
            }

            if (autoList.Count > 0)
            {
                lbSuggestType.ItemsSource = autoList;
                lbSuggestType.Visibility = Visibility.Visible;
            }
            else if (typeObjetObjet.Text.Equals(""))
            {
                lbSuggestType.Visibility = Visibility.Collapsed;
                lbSuggestType.ItemsSource = null;
            }
            else
            {
                lbSuggestType.Visibility = Visibility.Collapsed;
                lbSuggestType.ItemsSource = null;
            }


        }

        private void lbSuggesttionType_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestType.ItemsSource != null)
            {
                lbSuggestType.Visibility = Visibility.Collapsed;
                typeObjetObjet.TextChanged -= new TextChangedEventHandler(typeObjetObjet_TextChanged);
                if (lbSuggestType.SelectedIndex != -1)
                {
                    typeObjetObjet.Text = lbSuggestType.SelectedItems[0].ToString();
                }
                typeObjetObjet.TextChanged += new TextChangedEventHandler(typeObjetObjet_TextChanged);
            }
        }

        private void depotObjet_TextChanged(object sender, TextChangedEventArgs el)
        {
            string typeString = depotObjet.Text;
            List<string> autoList = new List<string>();
            autoList.Clear();
            foreach (DepotViewModel e in oDepot)
            {
                if (!string.IsNullOrEmpty(depotObjet.Text))
                {
                    if (e.nomDepotProperty.StartsWith(typeString))
                    {
                        autoList.Add(e.nomDepotProperty);
                    }
                }
            }

            if (autoList.Count > 0)
            {
                lbSuggestDepot.ItemsSource = autoList;
                lbSuggestDepot.Visibility = Visibility.Visible;
            }
            else if (depotObjet.Text.Equals(""))
            {
                lbSuggestDepot.Visibility = Visibility.Collapsed;
                lbSuggestDepot.ItemsSource = null;
            }
            else
            {
                lbSuggestDepot.Visibility = Visibility.Collapsed;
                lbSuggestDepot.ItemsSource = null;
            }
        }

        private void lbSuggesttionDepot_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestDepot.ItemsSource != null)
            {
                lbSuggestDepot.Visibility = Visibility.Collapsed;
                depotObjet.TextChanged -= new TextChangedEventHandler(depotObjet_TextChanged);
                if (lbSuggestDepot.SelectedIndex != -1)
                {
                    depotObjet.Text = lbSuggestDepot.SelectedItems[0].ToString();
                }
                depotObjet.TextChanged += new TextChangedEventHandler(depotObjet_TextChanged);
            }
        }
        public void objetButton_Click(object sender, RoutedEventArgs e)
        {
            MyDataObjet.idObjetProperty = ObjetDAL.getMaxIdObjet() + 1;
            MyDataObjet.nomObjetProperty = nomObjet.Text;
            MyDataObjet.artisteObjetProperty = artisteObjet.Text;
            MyDataObjet.prixDepartProperty = double.Parse(prixDepartObjet.Text);
            MyDataObjet.prixfinProperty = 0;
            MyDataObjet.prixFlashProperty = double.Parse(prixFlash.Text);
            MyDataObjet.prixMinEnchereProperty = double.Parse((prixMinEnchere.Text));
            MyDataObjet.venduProperty = 0;

            MyDataObjet.vendeurObjetProperty = UserORM.getUser(utilisateurObjet.Text);
            MyDataObjet.lotObjetProperty = LotORM.getLot(lotObjet.Text);
            MyDataObjet.depotObjetProperty = DepotORM.getDepot(depotObjet.Text);
            MyDataObjet.typeObjetProperty = TypeObjetORM.getTypeObjet(typeObjetObjet.Text);
            MyDataObjet.phraseVenduProperty = "A vendre";

            oObjet.Add(MyDataObjet);
            ObjetORM.insertObjet(MyDataObjet);

            listeObjet.Items.Refresh();

            //lbSuggestObjetOA.Items.Refresh();
            MyDataObjet = new ObjetViewModel();

            nomObjet.DataContext = MyDataObjet;
            artisteObjet.DataContext = MyDataObjet;
            prixDepartObjet.DataContext = MyDataObjet;
            prixFlash.DataContext = MyDataObjet;
            typeObjetObjet.DataContext = MyDataObjet;
            lotObjet.DataContext = MyDataObjet;
            utilisateurObjet.DataContext = MyDataObjet;
            depotObjet.DataContext = MyDataObjet;
            prixMinEnchere.DataContext = MyDataObjet;

        }


        private void supprimerButton_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listeObjet.SelectedItem is ObjetViewModel)
            {
                try
                {
                    ObjetORM.supprimerObjet(idObjet);
                    ObjetViewModel toRemove = (ObjetViewModel)listeObjet.SelectedItem;
                    oObjet.Remove(toRemove);
                    listeObjet.Items.Refresh();
                    ObjetORM.supprimerObjet(idObjet);
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Vous ne pouvez pas supprimer cette objet" + exception);
                }

            }
        }
        private void listeObjet_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((listeObjet.SelectedIndex < oObjet.Count) && (listeObjet.SelectedIndex >= 0))
            {
                idObjet = (oObjet.ElementAt<ObjetViewModel>(listeObjet.SelectedIndex)).idObjetProperty;
            }
        }

    }
}
