﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace projet
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
        public static string onglet;

        public MainWindow()
		{
			System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);


			InitializeComponent();
			DALconnection.OpenConnection();

		}
        

        private void utilisateurButton_OnClick(object sender, RoutedEventArgs e)
        {
            DataContext = new UserViewModel();
        }
        private void ObjetButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataContext = new ObjetViewModel();
        }
        private void LotButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataContext = new LotViewModel();
        }
        private void TypeObjetButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataContext = new TypeObjetView();
        }
        private void OrdreAchatButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataContext = new OrdreAchatViewModel();
        }
        private void EnchereButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataContext = new EnchereViewModel();
        }
        private void DepotButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataContext = new DepotViewModel();
        }
        private void LieuEnchereButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataContext = new LieuEnchereViewModel();
        }

        private void VenteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataContext = new VenteViewModel();
        }
    }
}
