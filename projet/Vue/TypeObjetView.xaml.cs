﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace projet
{
    /// <summary>
    /// Logique d'interaction pour TypeObjetView.xaml
    /// </summary>
    public partial class TypeObjetView : UserControl
    {
        TypeObjetViewModel myDataTypeObjet;
        private ObservableCollection<TypeObjetViewModel> oTypeObjet;

        private int idTypeObjet;
        public TypeObjetView()
        {
            InitializeComponent();
            loadTypeObjet();
        }

        public void loadTypeObjet()
        {
            oTypeObjet = TypeObjetORM.listeTypeObjet();
            myDataTypeObjet = new TypeObjetViewModel();
            listeTypeObjet.ItemsSource = oTypeObjet;
        }
        public void typeObjetButton_Click(object sender, RoutedEventArgs e)
        {
            myDataTypeObjet.idTOProperty = TypeObjetDAL.getMaxIdTypeObjet() + 1;

            myDataTypeObjet.typeObjetProperty = typeObjet.Text;

            oTypeObjet.Add(myDataTypeObjet);
            TypeObjetORM.insertTypeObjet(myDataTypeObjet);

            listeTypeObjet.Items.Refresh();
            myDataTypeObjet = new TypeObjetViewModel();

            typeObjet.DataContext = myDataTypeObjet;
        }
        private void supprimerButton_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listeTypeObjet.SelectedItem is TypeObjetViewModel)
            {
                try
                {
                    TypeObjetORM.supprimerTypeObjet(idTypeObjet);
                    TypeObjetViewModel toRemove = (TypeObjetViewModel)listeTypeObjet.SelectedItem;
                    oTypeObjet.Remove(toRemove);
                    listeTypeObjet.Items.Refresh();
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Vous ne pouvez pas supprimer cette catégorie." + exception);
                }

            }
        }
        private void listeTypeObjet_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((listeTypeObjet.SelectedIndex < oTypeObjet.Count) && (listeTypeObjet.SelectedIndex >= 0))
            {
                idTypeObjet = (oTypeObjet.ElementAt<TypeObjetViewModel>(listeTypeObjet.SelectedIndex)).idTOProperty;
            }
        }
    }
}
