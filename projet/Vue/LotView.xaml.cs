﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace projet
{
    /// <summary>
    /// Logique d'interaction pour LotView.xaml
    /// </summary>
    public partial class LotView : UserControl
    {
        private LotViewModel myDataLot;
        private ObservableCollection<LotViewModel> oLot;

        private int idLot;
        public LotView()
        {
            InitializeComponent();
            loadLot();
        }

        private void loadLot()
        {
            oLot = LotORM.listeLot();
            myDataLot = new LotViewModel();
            listeLot.ItemsSource = oLot;
        }

        private void lotButton_Click(object sender, RoutedEventArgs e)
        {
            //myDataLot.dateVenteProperty = dateVenteLot.Text;
            myDataLot.descriptionLotProperty = descriptionLot.Text;
            myDataLot.nomLotProperty = nomLot.Text;
            myDataLot.idLotProperty = LotDAL.getMaxIdLot() + 1;

            oLot.Add(myDataLot);
            LotORM.insertLot(myDataLot);

            listeLot.Items.Refresh();
            myDataLot = new LotViewModel();

            nomLot.DataContext = myDataLot;
            descriptionLot.DataContext = myDataLot;
            //dateVenteLot.DataContext = myDataLot;
        }
        private void supprimerButton_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listeLot.SelectedItem is LotViewModel)
            {
                try
                {
                    LotORM.supprimerLot(idLot);
                    LotViewModel toRemove = (LotViewModel)listeLot.SelectedItem;
                    oLot.Remove(toRemove);
                    listeLot.Items.Refresh();
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Vous ne pouvez pas supprimer ce lot, il contient encore des objets." + exception);
                }
                
            }
        }
        private void listeLot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((listeLot.SelectedIndex < oLot.Count) && (listeLot.SelectedIndex >= 0))
            {
                idLot = (oLot.ElementAt<LotViewModel>(listeLot.SelectedIndex)).idLotProperty;
            }
        }


    }
}
