﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace projet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class UserView : UserControl
    {


        public static string onglet;
        int selectedUserId;
        
        UserViewModel myDataObject; // Objet de liaison avec la vue lors de l'ajout d'une User par exemple.
        ObservableCollection<UserViewModel> listep;
        //int compteur = 0;

        //int selectedLieuEnchereId;
        //int selectedAdresseId;

        public UserView()
        {
            InitializeComponent();
            // Initialisation de la liste des Users via la BDD.
            loadUsers();
            gererEventSupplémentaires();
        }

        /*   public void prenomChanged(object sender, TextChangedEventArgs e)
          {

          }

          public void nomChanged(object sender, TextChangedEventArgs e)
          {
          }

         //public void DateChanged(object sender, TextChangedEventArgs e){}

          public void NumTelChanged(object sender, TextChangedEventArgs e)
          {
          }

          public void AdresseMailChanged(object sender, TextChangedEventArgs e)
          {
          }

          public void RegionChanged(object sender, TextChangedEventArgs e)
          {
          }

          public void DepartementChanged(object sender, TextChangedEventArgs e)
          {
          }

          public void VilleChanged(object sender, TextChangedEventArgs e)
          {
          }

          public void AdresseChanged(object sender, TextChangedEventArgs e)
          {
          }

          public void NumeroChanged(object sender, TextChangedEventArgs e)
          {
              myDataObject.NumeroUserProperty = int.Parse(NumeroTextBox.Text);
          }*/

        public void CheckBoxChanged(object sender, RoutedEventArgs routedEventArgs)
        {
            if (CheckBoxAcheteur.IsChecked == true)
            {
                myDataObject.TypeUserProperty = 1;
            }

            if (CheckBoxVendeur.IsChecked == true)
            {
                myDataObject.TypeUserProperty = 2;
            }

            if (CheckBoxComissairePriseur.IsChecked == true)
            {
                MessageBoxResult result =
                    MessageBox.Show("Vous avez selectionné le type commissaire priseur êtes vous sur?", "Attention",
                        MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        myDataObject.TypeUserProperty = 3;
                        break;


                    case MessageBoxResult.No:
                        myDataObject.TypeUserProperty = 0;
                        CheckBoxComissairePriseur.DataContext = myDataObject;
                        CheckBoxVendeur.DataContext = myDataObject;
                        CheckBoxAcheteur.DataContext = myDataObject;
                        break;
                }

            }

            if (CheckBoxAcheteur.IsChecked == true && CheckBoxVendeur.IsChecked == true)
            {
                myDataObject.TypeUserProperty = 4;
            }


            if (CheckBoxComissairePriseur.IsChecked == true && CheckBoxAcheteur.IsChecked == true)
            {
                myDataObject.TypeUserProperty =5;
            }


            if (CheckBoxComissairePriseur.IsChecked == true && CheckBoxVendeur.IsChecked == true)
            {
                myDataObject.TypeUserProperty = 6;
            }


            if (CheckBoxAcheteur.IsChecked == true && CheckBoxVendeur.IsChecked == true &&
                CheckBoxComissairePriseur.IsChecked == true)
            {
                myDataObject.TypeUserProperty = 7;
            }
        }

        private void UserButton_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // On ne fait rien
        }


        

        private void UserButton_Click(object sender, RoutedEventArgs e)
        {
            myDataObject.idUserProperty = UserDAL.getMaxIdUser() + 1;
            myDataObject.DateDeNaissUserProperty = DateDeNaissTextBox.Text;
            myDataObject.NuméroDeTelUserProperty = int.Parse(NumérodeTelTextBox.Text);
            myDataObject.AdresseMailUserProperty = AdresseMailTextBox.Text;
            myDataObject.adresseUserProperty = new AdresseViewModel(AdresseDAL.getMaxIdAdresse() + 1, RegionTextBox.Text, DepartementTextBox.Text, VilleTextBox.Text, RueTextBox.Text, int.Parse(NumeroTextBox.Text));
            myDataObject.prenomUserProperty = prenomTextBox.Text;
            myDataObject.nomUserProperty = nomTextBox.Text;

            listep.Add(myDataObject);
            AdresseORM.insertAdresse(myDataObject.adresseUserProperty);
            UserORM.insertUser(myDataObject);

            listeUsers.Items.Refresh();
            myDataObject = new UserViewModel();

            nomTextBox.DataContext = myDataObject;
            prenomTextBox.DataContext = myDataObject;
            DateDeNaissTextBox.DataContext = myDataObject;
            NumérodeTelTextBox.DataContext = myDataObject;
            AdresseMailTextBox.DataContext = myDataObject;
            RegionTextBox.DataContext = myDataObject;
            DepartementTextBox.DataContext = myDataObject;
            VilleTextBox.DataContext = myDataObject;
            RueTextBox.DataContext = myDataObject;
            NumeroTextBox.DataContext = myDataObject;
            CheckBoxAcheteur.DataContext = myDataObject;
            CheckBoxVendeur.DataContext = myDataObject;
            CheckBoxComissairePriseur.DataContext = myDataObject;
            nomPrenomButton2.DataContext = myDataObject;
        }


        

        

        private void supprimerButton_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listeUsers.SelectedItem is UserViewModel)
            {
                try
                {
                    int idAdresse = UserORM.getUser(selectedUserId).adresseUserProperty.idAdresseProperty;
                    UserORM.supprimerUser(selectedUserId);
                    AdresseORM.supprimerAdresse(idAdresse);
                    UserViewModel toRemove = (UserViewModel)listeUsers.SelectedItem;
                    listep.Remove(toRemove);
                    listeUsers.Items.Refresh();
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Vous ne pouvez pas supprimer cette utilisateur." + exception);
                }
            }
        }


        private void DepotButton_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }




        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }


        private void listeUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((listeUsers.SelectedIndex < listep.Count) && (listeUsers.SelectedIndex >= 0))
            {
                selectedUserId = (listep.ElementAt<UserViewModel>(listeUsers.SelectedIndex)).idUserProperty;
            }
        }

        

        void loadUsers()
        {
            listep = UserORM.listeUsers();
            myDataObject = new UserViewModel();
            //LIEN AVEC la VIEW
            listeUsers.ItemsSource = listep; // bind de la liste avec la source, permettant le binding.
        }
       


      /*  void appliquerContexte()
        {
            // Lien avec les textbox
            nomTextBox.DataContext = myDataObject;
            prenomTextBox.DataContext = myDataObject;
            DateDeNaissTextBox.DataContext = myDataObject;
            NumérodeTelTextBox.DataContext = myDataObject;
            AdresseMailTextBox.DataContext = myDataObject;
            RegionTextBox.DataContext = myDataObject;
            DepartementTextBox.DataContext = myDataObject;
            VilleTextBox.DataContext = myDataObject;
            AdresseTextBox.DataContext = myDataObject;
            NumeroTextBox.DataContext = myDataObject;
            CheckBoxAcheteur.DataContext = myDataObject;



            // Lien entre age-slider et bouton
            nomPrenomButton2.DataContext = myDataObject;
            nomDpDpButton.DataContext = myDataObjectDepot;

            // Lien entre age et slider
            //txtAge.DataContext = myDataObject;
        }*/

        void gererEventSupplémentaires()
        {
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
    }
}
